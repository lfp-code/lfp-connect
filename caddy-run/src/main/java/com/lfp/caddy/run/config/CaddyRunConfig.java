package com.lfp.caddy.run.config;

import java.time.Duration;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.properties.converter.DurationConverter;

public interface CaddyRunConfig extends Config {

	@DefaultValue("7 days")
	@ConverterClass(DurationConverter.class)
	Duration caddyRebuildInterval();

	@DefaultValue("1 day")
	@ConverterClass(DurationConverter.class)
	Duration caddyRebuildDevInterval();

	@DefaultValue("false")
	boolean gsudoEnabledDev();

	String xcaddyVersion();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.properties());
	}

}
