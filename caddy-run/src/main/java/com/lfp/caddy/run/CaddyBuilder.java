package com.lfp.caddy.run;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.zeroturnaround.exec.ProcessResult;

import com.lfp.caddy.run.config.CaddyRunConfig;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.process.Procs;
import com.lfp.joe.process.Which;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

public class CaddyBuilder implements ThrowingSupplier<File, IOException> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 3;
	private static final FileExt BUILD_DIR = new FileExt(Utils.Files.tempDirectory(true), "caddy-build-" + VERSION);
	private static final FileExt X_CADDY_BUILD_ROOT_DIR;
	static {
		var dir = new FileExt(BUILD_DIR, "xcaddy");
		dir.mkdirs();
		X_CADDY_BUILD_ROOT_DIR = dir;
	}

	private final String[] modules;
	private final File caddyExec;
	private File _buildDirectory;

	public CaddyBuilder(Function<Bytes, Bytes> hashModifier, String... modules) {
		this.modules = modules;
		var rootDir = Utils.Files.tempFile(THIS_CLASS, "caddy-exec");
		String hashHex;
		{
			var hashParts = Utils.Lots.stream(rootDir.getAbsolutePath())
					.append(streamModules(modules))
					.toArray(Object[]::new);
			var hash = Utils.Crypto.hashMD5(hashParts);
			hash = modifyHash(hashModifier, hash);
			hashHex = hash.encodeHex();
		}
		this.caddyExec = new File(new File(rootDir, hashHex),
				String.format("%s-%s-%s%s", "caddy", hashHex, VERSION, Utils.Machine.isWindows() ? ".exe" : ""));

	}

	@Override
	public File get() throws IOException {
		return get(false);
	}

	protected File get(boolean locked) throws IOException {
		if (isExecutableValid(this.caddyExec)) {
			Procs.killAll(caddyExec.getName());
			return caddyExec;
		}
		var buildDirectory = getBuildDirectory();
		if (!locked) {
			var buildLock = new File(buildDirectory, "build.lock");
			return FileLocks.access(buildLock, fc -> {
				return get(true);
			}, StandardOpenOption.WRITE);
		}
		var caddyExecBuild = new File(buildDirectory, caddyExec.getName());
		if (!isExecutableValid(caddyExecBuild))
			build(caddyExecBuild);
		caddyExec.delete();
		FileUtils.copyFile(caddyExecBuild, caddyExec);
		Validate.isTrue(caddyExec.exists() && caddyExec.isFile(), "caddy build exec does not exist:%s",
				caddyExecBuild.getAbsolutePath());
		caddyExec.setExecutable(true);
		Validate.isTrue(caddyExec.canExecute(), "caddy exec is not executable:%s", caddyExec.getAbsolutePath());
		return caddyExec;
	}

	private void build(File caddyExecBuild) throws FileNotFoundException, IOException {
		var xcaddyExec = getXCaddy();
		List<String> fullCmd = Utils.Lots
				.stream(xcaddyExec.getAbsolutePath(), "build", "--output", caddyExecBuild.getAbsolutePath())
				.toList();
		for (String with : streamModules(modules)) {
			fullCmd.add("--with");
			fullCmd.add(with);
		}
		execute(getBuildDirectory(), fullCmd);
		Validate.isTrue(caddyExecBuild.exists() && caddyExecBuild.isFile(), "caddy build exec does not exist:%s",
				caddyExecBuild.getAbsolutePath());
	}

	private File getBuildDirectory() {
		if (_buildDirectory == null)
			synchronized (this) {
				if (_buildDirectory == null) {
					var buildDir = new File(BUILD_DIR, this.caddyExec.getName());
					buildDir.mkdirs();
					this._buildDirectory = buildDir;
				}
			}
		return _buildDirectory;
	}

	protected static File getXCaddy() throws FileNotFoundException, IOException {
		return getXCaddy(false);
	}

	protected static File getXCaddy(boolean locked) throws FileNotFoundException, IOException {
		var xcaddyExec = Which.get("xcaddy").orElse(null);
		if (isExecutableValid(xcaddyExec))
			return xcaddyExec;
		if (!locked) {
			return FileLocks.access(new File(X_CADDY_BUILD_ROOT_DIR, "lock.lfp"), fc -> {
				return getXCaddy(true);
			}, StandardOpenOption.WRITE);
		}
		var buildDir = new FileExt(X_CADDY_BUILD_ROOT_DIR, "build");
		buildDir.deleteAll();
		buildDir.mkdirs();
		var goExec = Which.get("go").orElse(null);
		Objects.requireNonNull(goExec, "go must be installed on system");
		var xcaddyVersion = Configs.get(CaddyRunConfig.class).xcaddyVersion();
		if (Utils.Strings.isBlank(xcaddyVersion))
			xcaddyVersion = "latest";
		var fullCmd = Arrays.asList(goExec.getAbsolutePath(), "install",
				"github.com/caddyserver/xcaddy/cmd/xcaddy@" + xcaddyVersion);
		execute(buildDir, fullCmd);
		try {
			xcaddyExec = Which.poll("xcaddy").get(15, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			throw new RuntimeException("xcaddy install failed", e);
		}
		return xcaddyExec;
	}

	protected static ProcessResult execute(File workingDir, Iterable<String> commands) {
		var cmdArr = Utils.Lots.stream(commands).toArray(String.class);
		var pe = Procs.processExecutor().command(cmdArr);
		pe.directory(workingDir);
		return Utils.Functions.unchecked(() -> pe.start().get());
	}

	private static Bytes modifyHash(Function<Bytes, Bytes> hashModifier, Bytes hash) {
		if (hashModifier == null)
			return hash;
		var modifiedHash = hashModifier.apply(hash);
		if (modifiedHash == null)
			return hash;
		return modifiedHash;
	}

	private static StreamEx<String> streamModules(String... modules) {
		var moduleStream = Utils.Lots.stream(modules);
		moduleStream = moduleStream.distinct();
		moduleStream = moduleStream.filter(Utils.Strings::isNotBlank);
		moduleStream = moduleStream.sorted();
		return moduleStream;
	}

	private static boolean isExecutableValid(File executable) {
		if (executable == null || !executable.exists())
			return false;
		long elapsed = System.currentTimeMillis() - executable.lastModified();
		var cfg = Configs.get(CaddyRunConfig.class);
		Duration ttl = !MachineConfig.isDeveloper() ? cfg.caddyRebuildInterval() : cfg.caddyRebuildDevInterval();
		if (ttl.toMillis() < elapsed)
			return false;
		return true;
	}

	public static void main(String[] args)
			throws IOException, IllegalArgumentException, InterruptedException, ExecutionException {
		var file = new CaddyBuilder(null).get();
		Procs.start(StreamEx.of(file.getAbsolutePath(), "help").joining(" "), pem -> pem.logOutput()).get();
		Procs.start(StreamEx.of(file.getAbsolutePath(), "build-info").joining(" "), pem -> pem.logOutput()).get();
	}

}
