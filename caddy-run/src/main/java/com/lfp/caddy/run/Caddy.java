package com.lfp.caddy.run;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import org.slf4j.Logger;

import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.caddy.run.config.CaddyRunConfig;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.core.lock.FileLocks;
import com.lfp.joe.core.log.Logging;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.config.NetConfig;
import com.lfp.joe.process.ProcessExecutorLFP;
import com.lfp.joe.process.ProcessLFP;
import com.lfp.joe.process.Procs;
import com.lfp.joe.process.Which;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import ch.qos.logback.classic.Level;

public class Caddy {

	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 2;

	public static ProcessLFP start(Function<File, CaddyConfig> caddyConfigFunction, String... modules) {
		var caddyExec = getExecutable(modules);
		if (caddyConfigFunction == null)
			caddyConfigFunction = CaddyConfigs::getDefault;
		var caddyConfig = caddyConfigFunction == null ? null : caddyConfigFunction.apply(caddyExec);
		return start(caddyExec, caddyConfig);
	}

	public static ProcessLFP start(File caddyExec, CaddyConfig caddyConfig) {
		NetConfig.init();
		Objects.requireNonNull(caddyExec);
		if (caddyConfig == null)
			caddyConfig = CaddyConfigs.getDefault(caddyExec);
		var storageModule = Optional.ofNullable(caddyConfig)
				.map(v -> v.getStorage())
				.map(v -> v.getModule())
				.orElse(null);
		if (Utils.Strings.isBlank(storageModule))
			caddyConfig = CaddyConfigs.withStorageFileSystem(caddyConfig,
					new File(caddyExec.getParentFile(), "storage"));
		var caddyConfigBytes = Serials.Gsons.toBytes(caddyConfig);
		var hashHex = Utils.Crypto.hashMD5(caddyConfigBytes).encodeHex();
		var diskDir = Utils.Files.tempFile(THIS_CLASS, VERSION, hashHex);
		var configFile = new FileExt(diskDir, "config.json");
		if (!configFile.exists()) {
			diskDir.mkdirs();
			var lockFile = new FileExt(diskDir, "lock.lfp");
			try {
				FileLocks.write(lockFile, (fc, nil) -> {
					if (!configFile.exists()) {
						try (var fos = new FileOutputStream(configFile); var is = caddyConfigBytes.inputStream()) {
							is.transferTo(fos);
						}
					}
					return Nada.get();
				});
			} catch (IOException e) {
				configFile.delete();
				throw new RuntimeException(e);
			}
		}
		var cmds = Arrays.asList(caddyExec.getAbsolutePath(), "run", "--config", configFile.getAbsolutePath());
		var scriptElevatorOp = getScriptElevator();
		if (scriptElevatorOp.isPresent())
			cmds = Utils.Lots.stream(cmds).prepend(scriptElevatorOp.get().getAbsolutePath()).toList();
		var pe = Procs.processExecutor().command(cmds);
		return Throws.unchecked(() -> pe.start());
	}

	public static void configureLogging(ProcessExecutorLFP processExecutor) {
		configureLogging(processExecutor, null);
	}

	@SuppressWarnings("deprecation")
	public static void configureLogging(ProcessExecutorLFP processExecutor, Logger processLogger) {
		Supplier<Logger> loggerGetter = () -> processLogger != null ? processLogger : logger;
		processExecutor.onOutput((ot, line) -> {
			Level level = null;
			if (Utils.Strings.startsWith(line, "{") && Utils.Strings.endsWith(line, "}")) {
				var lineJson = Utils.Functions.catching(() -> Serials.Gsons.getJsonParser().parse(line), t -> null);
				level = Serials.Gsons.tryGetAsString(lineJson, "level").flatMap(v -> Logging.toLevel(v)).orElse(null);
			}
			if (level != null && !level.isGreaterOrEqual(Level.INFO) && (MachineConfig.isDeveloper()))
				level = Level.INFO;
			if (level == null)
				loggerGetter.get().info(line);
			else
				Logging.levelConsumer(loggerGetter.get(), level).accept(line, null);
		});
	}

	public static File getExecutable(ProcessLFP process) {
		if (process == null)
			return null;
		var commandStream = Utils.Lots.stream(process.getAttributes().getCommand()).filter(Utils.Strings::isNotBlank);
		var fileStream = commandStream.map(v -> {
			return Utils.Functions.catching(() -> {
				var file = new File(v);
				if (!file.exists() || !file.isFile() || !file.canExecute())
					return null;
				return file;
			}, t -> null);
		});
		fileStream = fileStream.nonNull();
		var scriptElevatorOp = getScriptElevator();
		if (scriptElevatorOp.isPresent())
			fileStream = fileStream.filter(v -> {
				return !scriptElevatorOp.get().getAbsolutePath().equals(v.getAbsolutePath());
			});
		return fileStream.findFirst().orElse(null);
	}

	public static File getExecutable(String... modules) {
		return getExecutable(null, modules);
	}

	public static File getExecutable(Function<Bytes, Bytes> hashModifier, String... modules) {
		var caddyBuilder = new CaddyBuilder(hashModifier, modules);
		return Utils.Functions.unchecked(() -> caddyBuilder.get());
	}

	private static Optional<File> getScriptElevator() {
		if (!MachineConfig.isDeveloper())
			return Optional.empty();
		if (Configs.get(CaddyRunConfig.class).gsudoEnabledDev()) {
			var gsudoOp = Which.get("gsudo");
			if (gsudoOp.isPresent())
				return gsudoOp;
		}
		return Optional.empty();
	}

}
