package test;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.caddy.core.config.Apps;
import com.lfp.caddy.core.config.Automation;
import com.lfp.caddy.core.config.Certificates;
import com.lfp.caddy.core.config.ConnectionPolicy;
import com.lfp.caddy.core.config.HandleLayer4;
import com.lfp.caddy.core.config.Issuer;
import com.lfp.caddy.core.config.Layer4;
import com.lfp.caddy.core.config.MatchLayer4;
import com.lfp.caddy.core.config.MatchTls;
import com.lfp.caddy.core.config.Policy;
import com.lfp.caddy.core.config.RouteLayer4;
import com.lfp.caddy.core.config.ServerLayer4;
import com.lfp.caddy.core.config.Tls;
import com.lfp.caddy.run.Caddy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;

public class Layer4Test {
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		Map<String, Integer> layer4Maps = new HashMap<>();
		layer4Maps.put("tendis.127.0.0.1.nip.io", 51002);
		layer4Maps.put("webserver.127.0.0.1.nip.io", 8282);
		layer4Maps.put("mongodb.127.0.0.1.nip.io", 27017);
		int frontendPort = 443;
		var caddyConfigBuilder = CaddyConfigs.withLoggingLevel(CaddyConfig.builder().build(), Level.DEBUG).toBuilder();
		var appsBuilder = Apps.builder();
		{// tls
			var certificates = Certificates.builder().automate(Utils.Lots.stream(layer4Maps.keySet()).toList()).build();
			Policy policy = Policy.builder().keyType("rsa4096").issuers(Issuer.builder().module("internal").build())
					.build();
			var tls = Tls.builder().automation(Automation.builder().policies(policy).build()).certificates(certificates)
					.build();
			appsBuilder.tls(tls);
		}
		List<RouteLayer4> routes = new ArrayList<>();
		for (var ent : layer4Maps.entrySet()) {// layer4
			var hostname = ent.getKey();
			int backendPort = ent.getValue();
			var matchTls = MatchTls.builder().sni(hostname).build();
			List<ConnectionPolicy> connectionPolicies;
			if (Utils.Strings.containsIgnoreCase(hostname, "web"))
				connectionPolicies = List.of(ConnectionPolicy.builder().alpn("http/1.1").build());
			else
				connectionPolicies = List.of();
			var tlsHandleLayer4 = HandleLayer4.builder().handler("tls").connectionPolicies(connectionPolicies).build();
			var proxyHandleLayer4 = CaddyConfigs.toProxyHandleLayer4(new InetSocketAddress("localhost", backendPort));
			var routeLayer4 = RouteLayer4.builder().match(MatchLayer4.builder().tls(matchTls).build())
					.handle(tlsHandleLayer4, proxyHandleLayer4).build();
			routes.add(routeLayer4);
		}
		var serverLayer4 = ServerLayer4.builder().listen("0.0.0.0:" + frontendPort).routes(routes).build();
		var layer4 = Layer4.builder().servers(Map.of("server_0", serverLayer4)).build();
		appsBuilder.layer4(layer4);
		var caddyConfig = caddyConfigBuilder.apps(appsBuilder.build()).build();
		System.out.println(Serials.Gsons.getPretty().toJson(caddyConfig));
		var process = Caddy.start(nil -> caddyConfig,
				"github.com/mholt/caddy-l4@6587f40d4eb632d15f295cf9791d7d145434d0da");
		process.getFuture().get();
	}
}
