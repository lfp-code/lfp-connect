package test;

import com.lfp.joe.process.Procs;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Map;

import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.config.Apps;
import com.lfp.caddy.core.config.Handle;
import com.lfp.caddy.core.config.Http;
import com.lfp.caddy.core.config.MatchHttp;
import com.lfp.caddy.core.config.Route;
import com.lfp.caddy.core.config.Server;
import com.lfp.caddy.core.config.Tls;
import com.lfp.caddy.core.config.Upstream;
import com.lfp.caddy.run.Caddy;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

public class TestJson {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws IOException, IllegalArgumentException, InterruptedException {
		var appsBuilder = Apps.builder();
		appsBuilder.http(buildHttp());
		appsBuilder.tls(buldTls());
		var caddyConfigBuilder = CaddyConfig.builder().apps(appsBuilder.build());
		var caddyConfig = caddyConfigBuilder.build();
		System.out.println(Serials.Gsons.getPretty().toJson(caddyConfig));
		File tempFile = Utils.Files.tempFile(THIS_CLASS, "v1",
				"config." + Utils.Crypto.getSecureRandomString() + ".json");
		Files.write(tempFile.toPath(), Serials.Gsons.toBytes(caddyConfig).array());
		Utils.Functions.unchecked(() -> Procs.start("taskkill", "/f", "/im", "caddy.exe").block());
		var exec = Caddy.getExecutable();
		var proc = Utils.Process
				.start(Arrays.asList(exec.getAbsolutePath(), "run", "--config", tempFile.getAbsolutePath()), pve -> {
					pve.logOutput();
				});
		proc.block(true);
	}

	private static Tls buldTls() {
		return null;
	}

	private static Http buildHttp() {
		var builder = Http.builder();
		builder.httpPort(8880);
		builder.servers(Map.of("server_0", buildServer()));
		return builder.build();
	}

	private static Server buildServer() {
		var builder = Route.builder();
		builder.match(MatchHttp.builder().host("localhost").build());
		builder.handle(buildHandle());
		return Server.builder().listen("0.0.0.0:8443").routes(builder.build()).build();
	}

	private static Handle buildHandle() {
		var builder = Handle.builder();
		builder.handler("reverse_proxy");
		builder.upstreams(Upstream.builder().dial("localhost:8282").build());
		return builder.build();
	}

}
