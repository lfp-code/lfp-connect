package test;

import java.time.Duration;
import java.util.Date;
import java.util.List;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.retrofit.AbstractJwkManagerHandler;
import com.lfp.joe.jwk.JwkManagerEndpoint;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import io.undertow.server.HttpServerExchange;

public class DevTokenHandler extends AbstractJwkManagerHandler {

	public DevTokenHandler(JwkManagerEndpoint jwkManagerEndpoint) {
		super(jwkManagerEndpoint);
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		System.out.println(
				Serials.Gsons.getPretty().toJson(UndertowUtils.streamHeaders(exchange.getRequestHeaders()).grouping()));

		var jwtBuilder = this.getJwkManagerEndpoint().getJwkManager().newJwtBuilder();
		jwtBuilder.setExpiration(new Date(System.currentTimeMillis() + Duration.ofMinutes(2).toMillis()));
		var claimCount = Utils.Crypto.getRandomInclusive(5, 20);
		for (int i = 0; i < claimCount; i++)
			jwtBuilder.claim("claim_" + i, "this is a test");
		exchange.getResponseSender().send(jwtBuilder.compact());
	}

	@Override
	protected Iterable<String> getPaths() {
		return List.of("/dev-token/");
	}

}
