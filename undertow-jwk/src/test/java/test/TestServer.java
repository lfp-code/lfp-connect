package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.function.Supplier;

import org.threadly.concurrent.future.ListenableFuture;

import com.google.common.reflect.TypeToken;
import com.lfp.connect.undertow.Undertows;
import com.lfp.connect.undertow.handler.ErrorLoggingHandler;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.connect.undertow.retrofit.JwkManagerHandlers;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.jwk.AbstractJwkManager;
import com.lfp.joe.jwk.JwkManager;
import com.lfp.joe.jwk.JwkManagerEndpoint;
import com.lfp.joe.jwk.JwkRecord;
import com.lfp.joe.jwk.Jwks;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.function.Semaphore;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.nimbusds.jose.jwk.JWK;

import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import one.util.streamex.StreamEx;

public class TestServer {

	public static final String ISSUER_URL = "https://reggie-pierce-dev.iplasso.com";
	private static final TypeToken<List<JwkRecord>> TT = new TypeToken<List<JwkRecord>>() {
	};

	@SuppressWarnings("resource")
	public static void main(String[] args) throws InterruptedException {
		var semaphore = Semaphore.create(1);
		File file = new File("temp/jwk-manager-test.json");
		file.getParentFile().mkdirs();
		Supplier<JWK> jwkGenerator = () -> Jwks.createECKey();
		JwkManager manager = new AbstractJwkManager(ISSUER_URL, Duration.ofSeconds(5), Duration.ofSeconds(90), jwkGenerator) {

			private ListenableFuture<Nada> future;

			@Override
			protected Iterable<JwkRecord> read() throws IOException {
				if (!file.exists())
					return null;
				try (var fis = new FileInputStream(file)) {
					return Serials.Gsons.fromStream(fis, TT);
				}
			}

			@Override
			protected void write(StreamEx<JwkRecord> jwks) throws IOException {
				if (future != null && !future.isDone())
					return;
				System.out.println("updating keys");
				var pool = Threads.Pools.centralPool();
				var future = pool.submit(() -> {
					var list = jwks.toList();
					try (var fos = new FileOutputStream(file)) {
						Serials.Gsons.toStream(list, fos);
					}
					return Nada.get();
				});
				Threads.Futures.logFailureError(future, true, "error");
				this.future = future;

			}

			@Override
			protected <X> X lockAccess(ThrowingSupplier<X, Exception> accessor) throws Exception {
				semaphore.acquire();
				try {
					return accessor.get();
				} finally {
					semaphore.release();
				}
			}
		};
		var jwkManagerEndpoint = new JwkManagerEndpoint(manager);
		PathHandler pathHandler = new PathHandler();
		JwkManagerHandlers.appendPaths(jwkManagerEndpoint, pathHandler);
		new DevTokenHandler(jwkManagerEndpoint).accept(pathHandler);
		HttpHandler handler = pathHandler;
		handler = new ThreadHttpHandler(handler);
		handler = new ErrorLoggingHandler(handler);
		var server = Undertows.serverBuilder(6969).setHandler(handler).build();
		server.start();
		System.out.println(server.getListenerInfo());
		Thread.currentThread().join();
	}

}
