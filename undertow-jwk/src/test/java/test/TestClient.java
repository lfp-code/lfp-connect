package test;

import com.lfp.joe.jwt.Jwts;
import com.lfp.joe.jwt.resolver.IssuerClaimSigningKeyResolver;
import com.lfp.joe.utils.Utils;

public class TestClient {

	public static void main(String[] args) {
		String url = TestServer.ISSUER_URL;
		String jwt = "eyJraWQiOiJqd2s5bmRvNnA5MGFoZTNscm5mdjR4bnNnZTI0IiwiYWxnIjoiRVM1MTIifQ.eyJpc3MiOiJodHRwczovL3JlZ2dpZS1waWVyY2UtZGV2LmlwbGFzc28uY29tLyIsImV4cCI6MTYzMTcyNjM2MSwiY2xhaW1fMCI6InRoaXMgaXMgYSB0ZXN0IiwiY2xhaW1fMSI6InRoaXMgaXMgYSB0ZXN0IiwiY2xhaW1fMiI6InRoaXMgaXMgYSB0ZXN0IiwiY2xhaW1fMyI6InRoaXMgaXMgYSB0ZXN0IiwiY2xhaW1fNCI6InRoaXMgaXMgYSB0ZXN0IiwiY2xhaW1fNSI6InRoaXMgaXMgYSB0ZXN0IiwiaWF0IjoxNjMxNzI2MjQxfQ.AGFdQwZES8q5hSWTKlAewDKubUe7fVkh5h8SGOZYDehR5QvjHJahJjGcJgeC67M7wbmE3u-DcMsr4PMrZgVSy7E5AFXdY-j_CcwHxUcs9FHqTh6OXVOdc3fxxv2t4fCLdWv5SLqvBFgZkJmyKCszdanQU8ktlUjJkm4cqdos-INSeoc0";
		var resolver = new IssuerClaimSigningKeyResolver(v -> {
			return Utils.Strings.containsIgnoreCase(v.toString(), url);
		});
		System.out.println(Jwts.tryParse(jwt, resolver).get());
	}

}
