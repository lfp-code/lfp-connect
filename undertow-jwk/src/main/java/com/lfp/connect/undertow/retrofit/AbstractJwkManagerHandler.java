package com.lfp.connect.undertow.retrofit;

import java.util.Objects;
import java.util.function.Consumer;

import com.google.common.net.MediaType;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.jwk.JwkManagerEndpoint;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.PathHandler;
import io.undertow.util.Headers;

public abstract class AbstractJwkManagerHandler implements HttpHandler, Consumer<PathHandler> {

	private final JwkManagerEndpoint jwkManagerEndpoint;

	public AbstractJwkManagerHandler(JwkManagerEndpoint jwkManagerEndpoint) {
		super();
		this.jwkManagerEndpoint = Objects.requireNonNull(jwkManagerEndpoint);
	}

	@Override
	public void accept(PathHandler pathHandler) {
		var pathStream = Utils.Lots.stream(getPaths()).nonNull().map(URIs::normalizePath).distinct();
		for (var path : pathStream)
			UndertowUtils.appendPath(pathHandler, path, this);
	}

	protected JwkManagerEndpoint getJwkManagerEndpoint() {
		return jwkManagerEndpoint;
	}

	protected void sendJsonBytes(Bytes bytes, HttpServerExchange hsx) {
		hsx.setStatusCode(StatusCodes.OK);
		hsx.setResponseContentLength(bytes.length());
		hsx.getResponseHeaders().put(Headers.CONTENT_TYPE, MediaType.JSON_UTF_8.toString());
		hsx.getResponseSender().send(bytes.buffer());
	}

	protected abstract Iterable<String> getPaths();

}
