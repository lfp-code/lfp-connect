package com.lfp.connect.undertow.retrofit;

import com.lfp.joe.jwk.JwkManagerEndpoint;

import io.undertow.server.handlers.PathHandler;

public class JwkManagerHandlers {

	public static PathHandler appendPaths(JwkManagerEndpoint jwkManagerEndpoint, PathHandler pathHandler) {
		new WellKnownJwkSetHandler(jwkManagerEndpoint).accept(pathHandler);
		new WellKnownOpenIDConfigurationHandler(jwkManagerEndpoint).accept(pathHandler);
		return pathHandler;
	}
}
