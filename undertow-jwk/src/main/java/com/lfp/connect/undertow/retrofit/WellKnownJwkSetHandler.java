package com.lfp.connect.undertow.retrofit;

import java.util.Arrays;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwk.JwkManagerEndpoint;
import com.lfp.joe.jwt.config.JwtConfig;

import io.undertow.server.HttpServerExchange;

public class WellKnownJwkSetHandler extends AbstractJwkManagerHandler {

	public WellKnownJwkSetHandler(JwkManagerEndpoint jwkManagerEndpoint) {
		super(jwkManagerEndpoint);
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		var responseBody = this.getJwkManagerEndpoint().getWellKnownJwkSetResponse();
		sendJsonBytes(responseBody, exchange);
		exchange.endExchange();
	}

	@Override
	protected Iterable<String> getPaths() {
		var path = Configs.get(JwtConfig.class).wellKnownJwkSetPath();
		return Arrays.asList(path);
	}
}
