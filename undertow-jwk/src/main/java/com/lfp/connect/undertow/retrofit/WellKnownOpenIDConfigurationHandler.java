package com.lfp.connect.undertow.retrofit;

import java.util.Arrays;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.jwk.JwkManagerEndpoint;
import com.lfp.joe.jwt.config.JwtConfig;

import io.undertow.server.HttpServerExchange;

public class WellKnownOpenIDConfigurationHandler extends AbstractJwkManagerHandler {

	public WellKnownOpenIDConfigurationHandler(JwkManagerEndpoint jwkManagerEndpoint) {
		super(jwkManagerEndpoint);
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		var requestURI = UndertowUtils.getRequestURI(exchange);
		var responseBody = this.getJwkManagerEndpoint().getWellKnownOpenIDConfigurationResponse(requestURI);
		sendJsonBytes(responseBody, exchange);
		exchange.endExchange();
	}

	@Override
	protected Iterable<String> getPaths() {
		var path = Configs.get(JwtConfig.class).wellKnownOpenIDConfigurationPath();
		return Arrays.asList(path);
	}
}
