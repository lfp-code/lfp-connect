package com.lfp.connect.socks5.server;

import java.util.List;

import sockslib.server.manager.User;
import sockslib.server.manager.UserManager;

public interface NoopUserManager extends UserManager {

	public static NoopUserManager INSTANCE = new NoopUserManager() {
	};

	@Override
	default User check(String username, String password) {
		throw new UnsupportedOperationException("check not supported");
	}

	@Override
	default void create(User user) {
		throw new UnsupportedOperationException("create not supported");
	}

	@Override
	default UserManager addUser(String username, String password) {
		throw new UnsupportedOperationException("addUser not supported");
	}

	@Override
	default void delete(String username) {
		throw new UnsupportedOperationException("delete not supported");

	}

	@Override
	default List<User> findAll() {
		throw new UnsupportedOperationException("findAll not supported");
	}

	@Override
	default void update(User user) {
		throw new UnsupportedOperationException("update not supported");

	}

	@Override
	default User find(String username) {
		throw new UnsupportedOperationException("find not supported");
	}

}
