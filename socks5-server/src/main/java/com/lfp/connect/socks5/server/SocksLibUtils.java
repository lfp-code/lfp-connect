package com.lfp.connect.socks5.server;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;
import com.lfp.joe.utils.Utils;

import sockslib.server.BasicSocksProxyServer;
import sockslib.server.MethodSelector;
import sockslib.server.Session;
import sockslib.server.Socks5Handler;
import sockslib.server.SocksSession;
import sockslib.server.UsernamePasswordAuthenticator;
import sockslib.server.manager.User;

@SuppressWarnings("unchecked")
public class SocksLibUtils {

	private static final FieldAccessor<BasicSocksProxyServer, MethodSelector> BasicSocksProxyServer_methodSelector_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(BasicSocksProxyServer.class, true, f -> "methodSelector".equals(f.getName()),
					f -> MethodSelector.class.isAssignableFrom(f.getType()));

	public static MethodSelector getMethodSelector(BasicSocksProxyServer basicSocksProxyServer) {
		return BasicSocksProxyServer_methodSelector_FA.get(basicSocksProxyServer);
	}

	private static final FieldAccessor<Socks5Handler, Session> Socks5Handler_session_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(Socks5Handler.class, true, f -> "session".equals(f.getName()),
					f -> Session.class.isAssignableFrom(f.getType()));

	public static SocksSession getSocksSession(Socks5Handler socks5Handler) {
		SocksSession socksSession = (SocksSession) Socks5Handler_session_FA.get(socks5Handler);
		return socksSession;
	}

	public static User getUser(Session session) {
		if (session == null)
			return null;
		var userObj = session.getAttribute(UsernamePasswordAuthenticator.USER_KEY);
		return Utils.Types.tryCast(userObj, User.class).orElse(null);
	}
}
