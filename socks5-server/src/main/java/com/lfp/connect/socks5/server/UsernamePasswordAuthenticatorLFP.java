package com.lfp.connect.socks5.server;

import com.lfp.joe.core.config.MachineConfig;

import sockslib.server.UsernamePasswordAuthenticator;
import sockslib.server.manager.User;

public abstract class UsernamePasswordAuthenticatorLFP extends UsernamePasswordAuthenticator {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public UsernamePasswordAuthenticatorLFP() {
		super();
		this.setUserManager(new NoopUserManager() {

			@Override
			public User check(String username, String password) {
				User user = getValidUser(username, password);
				if (user != null)
					return user;
				String message = String.format("user check failed. username:%s password:%s", username,
						MachineConfig.isDeveloper() ? password : "[redacted]");
				if (MachineConfig.isDeveloper())
					logger.warn(message);
				else
					logger.debug(message);
				return null;
			}
		});
	}

	protected abstract User getValidUser(String username, String password);

	protected static User asUser(String username, String password) {
		return new User(username, password);
	}
}
