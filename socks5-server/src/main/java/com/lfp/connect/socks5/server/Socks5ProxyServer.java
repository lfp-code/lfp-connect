package com.lfp.connect.socks5.server;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.function.Supplier;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Throws;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.spi.FilterReply;
import sockslib.client.Socks5;
import sockslib.client.SocksProxy;
import sockslib.common.UsernamePasswordCredentials;
import sockslib.server.BasicSessionManager;
import sockslib.server.BasicSocksProxyServer;
import sockslib.server.SessionManager;
import sockslib.server.Socks5Handler;
import sockslib.server.SocksHandler;
import sockslib.server.SocksSession;
import sockslib.server.io.SocketPipe;
import sockslib.server.listener.PipeInitializer;
import sockslib.server.manager.User;

public class Socks5ProxyServer extends BasicSocksProxyServer {
	private static final MemoizedSupplier<Void> LOG_INIT = MemoizedSupplier.create(() -> {
		LogFilter.addFilter(ctx -> {
			var level = ctx.getLoggerLevel();
			if (level.isGreaterOrEqual(Level.WARN))
				return FilterReply.NEUTRAL;
			var loggerName = ctx.getEvent().getLoggerName();
			if (SocketPipe.class.getName().equals(loggerName))
				return FilterReply.DENY;
			if (Utils.Strings.startsWithAny(loggerName, "sockslib.common", "sockslib.client", "sockslib.server")
					&& !MachineConfig.isDeveloper())
				return FilterReply.DENY;
			return FilterReply.NEUTRAL;
		});
	});
	private static final int DEFAULT_TIMEOUT = 0;
	private static final boolean DEFAULT_DAEMON = false;
	private static final Supplier<SessionManager> DEFAULT_SESSION_MANAGER_SUPPLIER = BasicSessionManager::new;
	private static final Supplier<PipeInitializer> DEFAULT_PIPE_INITIALIZER_SUPPLIER = () -> null;
	private Function<Optional<User>, SocksProxy> socksProxySelector;

	public Socks5ProxyServer(int port) {
		this("0.0.0.0", port);
	}

	public Socks5ProxyServer(String hostname, int port) {
		this(new InetSocketAddress(hostname, port), Threads.Pools.centralPool());
	}

	public Socks5ProxyServer(InetSocketAddress address, Executor executor) {
		super(Socks5Handler.class, executor == null ? null : Threads.Executors.asExecutorService(executor));
		LOG_INIT.get();
		Objects.requireNonNull(address);
		this.setBindAddr(address.getAddress());
		this.setBindPort(address.getPort());
		this.setTimeout(DEFAULT_TIMEOUT);
		this.setDaemon(DEFAULT_DAEMON);
		this.setSessionManager(DEFAULT_SESSION_MANAGER_SUPPLIER.get());
		this.setPipeInitializer(DEFAULT_PIPE_INITIALIZER_SUPPLIER.get());
	}

	public void setProxySelector(Function<Optional<User>, Proxy> proxySelector) {
		if (proxySelector == null)
			this.socksProxySelector = null;
		else
			this.socksProxySelector = cop -> {
				var proxy = proxySelector.apply(cop);
				return Throws.unchecked(() -> asSocksProxy(proxy));
			};
	}

	@Override
	public void setProxy(SocksProxy proxy) {
		if (proxy == null)
			this.socksProxySelector = null;
		else
			this.socksProxySelector = nil -> proxy;
	}

	@Override
	public void initializeSocksHandler(SocksHandler socksHandler) {
		socksHandler.setMethodSelector(SocksLibUtils.getMethodSelector(this));
		socksHandler.setBufferSize(getBufferSize());
		socksHandler.setSocksProxyServer(this);
		if (this.socksProxySelector != null)
			socksHandler.setProxy(new LazySocks5Proxy((Socks5Handler) socksHandler) {

				@Override
				protected SocksProxy createDelegate(SocksSession socksSession) {
					if (socksProxySelector == null)
						return null;
					var user = SocksLibUtils.getUser(socksSession);
					var socksProxy = socksProxySelector.apply(Optional.ofNullable(user));
					return socksProxy;
				}
			});
	}

	protected static SocksProxy asSocksProxy(Proxy proxy) throws UnknownHostException {
		if (proxy == null)
			return null;
		if (proxy.isAuthenticationEnabled())
			return new Socks5(proxy.getHostname(), proxy.getPort(),
					new UsernamePasswordCredentials(proxy.getUsername().orElse(""), proxy.getPassword().orElse("")));
		else
			return new Socks5(proxy.getHostname(), proxy.getPort());
	}

}
