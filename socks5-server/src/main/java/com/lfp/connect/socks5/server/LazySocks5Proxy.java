package com.lfp.connect.socks5.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Objects;

import sockslib.client.CommandReplyMessage;
import sockslib.client.SocksMethodRequester;
import sockslib.client.SocksProxy;
import sockslib.common.Credentials;
import sockslib.common.SocksException;
import sockslib.common.methods.SocksMethod;
import sockslib.server.Socks5Handler;
import sockslib.server.SocksSession;

public abstract class LazySocks5Proxy implements SocksProxy {

	private Socks5Handler socks5Handler;
	private SocksProxy _delegate;

	public LazySocks5Proxy(Socks5Handler socks5Handler) {
		this.socks5Handler = Objects.requireNonNull(socks5Handler);
	}

	protected SocksProxy getDelegate() {
		if (_delegate == null)
			synchronized (this) {
				if (_delegate == null)
					_delegate = createDelegate(SocksLibUtils.getSocksSession(this.socks5Handler));
			}
		return _delegate;
	}

	protected abstract SocksProxy createDelegate(SocksSession socksSession);

	@Override
	public Socket getProxySocket() {
		return getDelegate().getProxySocket();
	}

	@Override
	public SocksProxy setProxySocket(Socket socket) {
		return getDelegate().setProxySocket(socket);
	}

	@Override
	public int getPort() {
		return getDelegate().getPort();
	}

	@Override
	public SocksProxy setPort(int port) {
		return getDelegate().setPort(port);
	}

	@Override
	public InetAddress getInetAddress() {
		return getDelegate().getInetAddress();
	}

	@Override
	public SocksProxy setHost(String host) throws UnknownHostException {
		return getDelegate().setHost(host);
	}

	@Override
	public void buildConnection() throws IOException, SocksException {
		getDelegate().buildConnection();
	}

	@Override
	public CommandReplyMessage requestConnect(String host, int port) throws SocksException, IOException {
		return getDelegate().requestConnect(host, port);
	}

	@Override
	public CommandReplyMessage requestConnect(InetAddress address, int port) throws SocksException, IOException {
		return getDelegate().requestConnect(address, port);
	}

	@Override
	public CommandReplyMessage requestConnect(SocketAddress address) throws SocksException, IOException {
		return getDelegate().requestConnect(address);
	}

	@Override
	public CommandReplyMessage requestBind(String host, int port) throws SocksException, IOException {
		return getDelegate().requestBind(host, port);
	}

	@Override
	public CommandReplyMessage requestBind(InetAddress inetAddress, int port) throws SocksException, IOException {
		return getDelegate().requestBind(inetAddress, port);
	}

	@Override
	public Socket accept() throws SocksException, IOException {
		return getDelegate().accept();
	}

	@Override
	public CommandReplyMessage requestUdpAssociate(String host, int port) throws SocksException, IOException {
		return getDelegate().requestUdpAssociate(host, port);
	}

	@Override
	public CommandReplyMessage requestUdpAssociate(InetAddress address, int port) throws SocksException, IOException {
		return getDelegate().requestUdpAssociate(address, port);
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return getDelegate().getInputStream();
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		return getDelegate().getOutputStream();
	}

	@Override
	public Credentials getCredentials() {
		return getDelegate().getCredentials();
	}

	@Override
	public SocksProxy setCredentials(Credentials credentials) {
		return getDelegate().setCredentials(credentials);
	}

	@Override
	public List<SocksMethod> getAcceptableMethods() {
		return getDelegate().getAcceptableMethods();
	}

	@Override
	public SocksProxy setAcceptableMethods(List<SocksMethod> methods) {
		return getDelegate().setAcceptableMethods(methods);
	}

	@Override
	public SocksMethodRequester getSocksMethodRequester() {
		return getDelegate().getSocksMethodRequester();
	}

	@Override
	public SocksProxy setSocksMethodRequester(SocksMethodRequester requester) {
		return getDelegate().setSocksMethodRequester(requester);
	}

	@Override
	public int getSocksVersion() {
		return getDelegate().getSocksVersion();
	}

	@Override
	public SocksProxy copy() {
		return getDelegate().copy();
	}

	@Override
	public SocksProxy copyWithoutChainProxy() {
		return getDelegate().copyWithoutChainProxy();
	}

	@Override
	public SocksProxy getChainProxy() {
		return getDelegate().getChainProxy();
	}

	@Override
	public SocksProxy setChainProxy(SocksProxy chainProxy) {
		return getDelegate().setChainProxy(chainProxy);
	}

	@Override
	public Socket createProxySocket(InetAddress address, int port) throws IOException {
		return getDelegate().createProxySocket(address, port);
	}

	@Override
	public Socket createProxySocket() throws IOException {
		return getDelegate().createProxySocket();
	}
}
