package test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;

import com.lfp.connect.socks5.server.Socks5ProxyServer;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;

import sockslib.common.AuthenticationException;
import sockslib.common.Credentials;
import sockslib.common.methods.UsernamePasswordMethod;
import sockslib.server.Session;
import sockslib.server.UsernamePasswordAuthenticator;
import sockslib.server.manager.MemoryBasedUserManager;

public class ServerTest {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	protected static final Object CREDENTIALS_ATTRIBUTE_KEY = "credentials_" + Utils.Crypto.getSecureRandomString();

	public static void main(String[] args) throws InterruptedException, IOException {
		var address = new InetSocketAddress("0.0.0.0", 8888);
		var socks5ProxyServer = new Socks5ProxyServer(address, Threads.Pools.centralPool());
		socks5ProxyServer.setProxySelector(cop -> {
			Proxy proxy = Proxy.builder(URI.create(String.format("socks5://%s:%s@%s:%s", "reggiepierce",
					"Cl3aDfVc3DWeEUNLRf3CTydm5arz", "192.161.53.138", 61336))).build();
			return proxy;
		});
		MemoryBasedUserManager userManager = new MemoryBasedUserManager();
		userManager.addUser("user", "pass");
		var usernamePasswordAuthenticator = new UsernamePasswordAuthenticator(userManager) {

			@Override
			public void doAuthenticate(Credentials credentials, Session session) throws AuthenticationException {
				super.doAuthenticate(credentials, session);
				session.setAttribute(CREDENTIALS_ATTRIBUTE_KEY, credentials);
			}
		};
		socks5ProxyServer.setSupportMethods(new UsernamePasswordMethod(usernamePasswordAuthenticator));
		System.out.println("starting");
		socks5ProxyServer.start();
		System.out.println("started");
		Thread.currentThread().join();
	}

}
