#!/usr/bin/env bash

if [[ ! -z "${CADDY_CONFIG_JSON}" ]]; then
	if [ $(echo $CADDY_CONFIG_JSON | jq empty > /dev/null 2>&1; echo $?) -eq 0 ]; then
		echo "reading caddy json from text environment variaCONFIG_JSON_PATHble"
	else
		echo "reading caddy json from base64 environment variable"
		CADDY_CONFIG_JSON=$(echo $CADDY_CONFIG_JSON | base64 --decode)
	fi
	if [ $(echo $CADDY_CONFIG_JSON | jq empty > /dev/null 2>&1; echo $?) -eq 0 ]; then
		echo "creating caddy config from environment variable:${CADDY_CONFIG}"
		echo $CADDY_CONFIG_JSON > $CADDY_CONFIG
	fi
fi

$CADDY_EXEC $@
