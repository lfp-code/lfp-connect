TMP_CADDY_L4=./caddy-l4
if [ ! -f "$TMP_CADDY_L4/go.mod" ]; then
	echo "starting caddy-l4 clone"
    rm -rf $TMP_CADDY_L4
	mkdir $TMP_CADDY_L4
	git clone git@github.com:mholt/caddy-l4.git $TMP_CADDY_L4
else
	echo "skipping caddy-l4 clone"
fi

CMD="docker.exe"
if ! command -v docker.exe &> /dev/null
then
    CMD="docker"
fi

CMD="${CMD} build ${@}"
eval $CMD