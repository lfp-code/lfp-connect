package test;

import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.lfp.joe.utils.Utils;

public class VersionGen {

	private static final Date ORIGIN = Utils.Times.tryParseDate("2022").get();
	private static final List<Integer> MAX_VALS = List.of(65534, 255, 255);

	public static void main(String[] args) {
		var elapsedSeconds = Duration.ofMillis(System.currentTimeMillis() - ORIGIN.getTime()).getSeconds();
		var versionParts = Stream.iterate(0, v -> v < MAX_VALS.size(), v -> v + 1).map(v -> 0).toArray(Integer[]::new);
		while (elapsedSeconds > 0) {
			for (int i = 0;; i++) {
				versionParts[i] = versionParts[i] + 1;
				if (versionParts[i] < MAX_VALS.get(i)) {
					elapsedSeconds--;
					break;
				}
				versionParts[i] = 0;
			}
		}
		var version = Stream.<Integer>iterate(versionParts.length, v -> v > 0, v -> v - 1)
				.map(v -> versionParts[v])
				.map(Objects::toString)
				.collect(Collectors.joining("."));
		System.out.println(version);
	}
}
