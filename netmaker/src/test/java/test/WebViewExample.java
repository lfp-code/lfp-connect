package test;

import java.io.IOException;
import java.net.URL;

import com.dustinredmond.fxtrayicon.FXTrayIcon;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.utils.Utils;

import javafx.application.Application;
import javafx.concurrent.Worker;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import one.util.streamex.IntStreamEx;

public class WebViewExample extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		var iconURL = new URL(
				"https://e7.pngegg.com/pngimages/974/716/png-clipart-graphics-computer-icons-venue-icon-photography-logo.png");
		var title = new javafx.scene.control.MenuItem("Nectus");
		title.setDisable(true);
		FXTrayIcon icon = new FXTrayIcon.Builder(primaryStage,
				iconURL).toolTip("Nectus")
				.menuItem(title)
				.separator()
				.menuItem("Options", nil -> formView(primaryStage))
				.addExitMenuItem("Exit")
				.show()
				.build();

		System.out.println(CoreReflections.tryForName("com.sun.javafx.webkit.WebConsoleListene").orElse(null));
		primaryStage.setTitle("JavaFX WebView Example");

	}

	protected void formView(Stage primaryStage) {
		var textField = new TextField();
		textField.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(textField, Priority.ALWAYS);
		var button = new Button("OK");
		var hbox = new HBox(textField, button);
		hbox.setAlignment(Pos.CENTER);
		var vbox = new VBox(hbox);
		vbox.setPadding(new Insets(10));
		vbox.autosize();
		Scene scene = new Scene(vbox, 300, -1);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Test");
		primaryStage.show();
	}

	protected void webView(Stage primaryStage) {
		WebView webView = new WebView();
		webView.getEngine().getLoadWorker().stateProperty().addListener((value, oldValue, newValue) -> {
			if (Worker.State.SUCCEEDED != newValue)
				return;
			JSObject window = (JSObject) webView.getEngine()
					.executeScript(
							"window;");
			window.setMember("app", this);
			webView.getEngine()
					.executeScript("var button=document.createElement('a');\r\n"
							+ "function callback(){\r\n"
							+ "    window.app.test();\r\n"
							+ "}\r\n"
							+ "button.textContent=\"TEST\";\r\n"
							+ "button.onclick=callback\r\n"
							+ "document.body.prepend(button);");
		});
		webView.getEngine().load("http://example.com");
		MenuBar menuBar = new MenuBar();
		menuBar.useSystemMenuBarProperty().set(true);
		IntStreamEx.range(4).mapToObj(v -> "Menu " + v).map(Menu::new).forEach(menu -> {
			menuBar.getMenus().add(menu);
			IntStreamEx.range(Utils.Crypto.getRandomInclusive(1, 10))
					.mapToObj(v -> "Test " + v)
					.map(MenuItem::new)
					.forEach(menu.getItems()::add);
		});

		VBox vBox = new VBox(menuBar, webView);
		Scene scene = new Scene(vBox, 960, 600);

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public void test() {
		System.out.println("test");

	}

}