package test;

import org.apache.commons.net.util.SubnetUtils;

import com.lfp.joe.utils.Utils;

public class SubnetTest {

	public static void main(String[] args) {
		var subnet = String.format("69.%s.%s.0/24",
				Utils.Crypto.getRandomInclusive(1, 254),
				Utils.Crypto.getRandomInclusive(1, 254));
		var utils = new SubnetUtils(subnet);
		utils.setInclusiveHostCount(true);
		System.out.println(subnet);
		System.out.println(utils.getInfo().getLowAddress());
		System.out.println(utils.getInfo().getHighAddress());
	}
}
