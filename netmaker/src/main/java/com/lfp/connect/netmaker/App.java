package com.lfp.connect.netmaker;

public class App {

	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; !Thread.currentThread().isInterrupted(); i++) {
			System.out.println(i + " hello...");
			Thread.sleep(1_000);
		}
	}
}
