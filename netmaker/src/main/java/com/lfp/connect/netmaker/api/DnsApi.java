package com.lfp.connect.netmaker.api;

import com.lfp.connect.netmaker.invoker.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.lfp.connect.netmaker.model.DNSEntry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface DnsApi {
  /**
   * Create a DNS entry.
   * 
   * @param network Network (required)
   * @param body DNS Entry (optional)
   * @return Call&lt;List&lt;DNSEntry&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/dns/{network}")
  Call<List<DNSEntry>> createDNS(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Body List<DNSEntry> body
  );

  /**
   * Delete a DNS entry.
   * 
   * @param network Network (required)
   * @param domain Domain (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/dns/{network}/{domain}")
  Call<Void> deleteDNS(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("domain") String domain
  );

  /**
   * Gets all DNS entries.
   * 
   * @return Call&lt;List&lt;DNSEntry&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/dns")
  Call<List<DNSEntry>> getAllDNS();
    

  /**
   * Gets custom DNS entries associated with a network.
   * 
   * @param network Network (required)
   * @return Call&lt;List&lt;DNSEntry&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/dns/adm/{network}/custom")
  Call<List<DNSEntry>> getCustomDNS(
    @retrofit2.http.Path("network") String network
  );

  /**
   * Gets all DNS entries associated with the network.
   * 
   * @param network Network (required)
   * @return Call&lt;List&lt;DNSEntry&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/dns/adm/{network}")
  Call<List<DNSEntry>> getDNS(
    @retrofit2.http.Path("network") String network
  );

  /**
   * Gets node DNS entries associated with a network.
   * 
   * @param network Network (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/dns/adm/{network}/nodes")
  Call<Void> getNodeDNS(
    @retrofit2.http.Path("network") String network
  );

  /**
   * Push DNS entries to nameserver.
   * 
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/dns/adm/pushdns")
  Call<Void> pushDNS();
    

}
