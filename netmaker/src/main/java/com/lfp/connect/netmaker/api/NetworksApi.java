package com.lfp.connect.netmaker.api;

import com.lfp.connect.netmaker.invoker.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.lfp.connect.netmaker.model.ACLContainer;
import com.lfp.connect.netmaker.model.AccessKey;
import com.lfp.connect.netmaker.model.Network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface NetworksApi {
  /**
   * Create a network access key.
   * 
   * @param networkname Network Name (required)
   * @param accessKey Access Key (optional)
   * @return Call&lt;AccessKey&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/networks/{networkname}/keys")
  Call<AccessKey> createAccessKey(
    @retrofit2.http.Path("networkname") String networkname, @retrofit2.http.Body AccessKey accessKey
  );

  /**
   * Create a network.
   * 
   * @param network Network (optional)
   * @return Call&lt;Network&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/networks")
  Call<Network> createNetwork(
    @retrofit2.http.Body Network network
  );

  /**
   * Delete a network access key.
   * 
   * @param networkname Network Name (required)
   * @param accessKeyName Access Key Name (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/networks/{networkname}/keys/{name}")
  Call<Void> deleteAccessKey(
    @retrofit2.http.Path("networkname") String networkname, @retrofit2.http.Path("access_key_name") String accessKeyName
  );

  /**
   * Delete a network.  Will not delete if there are any nodes that belong to the network.
   * 
   * @param networkname Network Name (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/networks/{networkname}")
  Call<Void> deleteNetwork(
    @retrofit2.http.Path("networkname") String networkname
  );

  /**
   * Get network access keys for a network.
   * 
   * @param networkname Network Name (required)
   * @return Call&lt;List&lt;AccessKey&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/networks/{networkname}/keys")
  Call<List<AccessKey>> getAccessKeys(
    @retrofit2.http.Path("networkname") String networkname
  );

  /**
   * Get a network.
   * 
   * @param networkname Network Name (required)
   * @return Call&lt;Network&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/networks/{networkname}")
  Call<Network> getNetwork(
    @retrofit2.http.Path("networkname") String networkname
  );

  /**
   * Get a network ACL (Access Control List).
   * 
   * @param networkname Network Name (required)
   * @param aclContainer ACL Container (optional)
   * @return Call&lt;ACLContainer&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/networks/{networkname}/acls")
  Call<ACLContainer> getNetworkACL(
    @retrofit2.http.Path("networkname") String networkname, @retrofit2.http.Body ACLContainer aclContainer
  );

  /**
   * Lists all networks.
   * 
   * @param networks name: networks (optional)
   * @return Call&lt;List&lt;Network&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/networks")
  Call<List<Network>> getNetworks(
    @retrofit2.http.Header("networks") List<String> networks
  );

  /**
   * Update keys for a network.
   * 
   * @param networkname Network Name (required)
   * @return Call&lt;Network&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/networks/{networkname}/keyupdate")
  Call<Network> keyUpdate(
    @retrofit2.http.Path("networkname") String networkname
  );

  /**
   * Update a network.
   * 
   * @param networkname Network Name (required)
   * @param network Network (optional)
   * @return Call&lt;Network&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/networks/{networkname}")
  Call<Network> updateNetwork(
    @retrofit2.http.Path("networkname") String networkname, @retrofit2.http.Body Network network
  );

  /**
   * Update a network ACL (Access Control List).
   * 
   * @param networkname Network Name (required)
   * @param aclContainer ACL Container (optional)
   * @return Call&lt;ACLContainer&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/networks/{networkname}/acls")
  Call<ACLContainer> updateNetworkACL(
    @retrofit2.http.Path("networkname") String networkname, @retrofit2.http.Body ACLContainer aclContainer
  );

  /**
   * Update a network&#39;s node limit.
   * 
   * @param networkname Network Name (required)
   * @return Call&lt;Network&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/networks/{networkname}/nodelimit")
  Call<Network> updateNetworkNodeLimit(
    @retrofit2.http.Path("networkname") String networkname
  );

}
