package com.lfp.connect.netmaker.api;

import com.lfp.connect.netmaker.invoker.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.lfp.connect.netmaker.model.SuccessResponse;
import com.lfp.connect.netmaker.model.User;
import com.lfp.connect.netmaker.model.UserAuthParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface UserApi {
  /**
   * Node authenticates using its password and retrieves a JWT for authorization.
   * 
   * @param userAuthParams User Auth Params (optional)
   * @return Call&lt;SuccessResponse&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/users/adm/authenticate")
  Call<SuccessResponse> authenticateUser(
    @retrofit2.http.Body UserAuthParams userAuthParams
  );

  /**
   * Make a user an admin.
   * 
   * @param user User (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/users/adm/createadmin")
  Call<User> createAdmin(
    @retrofit2.http.Body User user
  );

  /**
   * Create a user.
   * 
   * @param username Username (required)
   * @param user User (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/users/{username}")
  Call<User> createUser(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Body User user
  );

  /**
   * Delete a user.
   * 
   * @param username Username (required)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/users/{username}")
  Call<User> deleteUser(
    @retrofit2.http.Path("username") String username
  );

  /**
   * Get an individual user.
   * 
   * @param username Username (required)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/users/{username}")
  Call<User> getUser(
    @retrofit2.http.Path("username") String username
  );

  /**
   * Get all users.
   * 
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/users")
  Call<User> getUsers();
    

  /**
   * Checks whether the server has an admin.
   * 
   * @return Call&lt;SuccessResponse&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/users/adm/hasadmin")
  Call<SuccessResponse> hasAdmin();
    

  /**
   * Update a user.
   * 
   * @param username Username (required)
   * @param user User (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/users/{username}")
  Call<User> updateUser(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Body User user
  );

  /**
   * Updates the given admin user&#39;s info (as long as the user is an admin).
   * 
   * @param username Username (required)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/users/{username}/adm")
  Call<User> updateUserAdm(
    @retrofit2.http.Path("username") String username
  );

  /**
   * Updates the networks of the given user.
   * 
   * @param username Username (required)
   * @param user User (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/users/networks/{username}")
  Call<User> updateUserNetworks(
    @retrofit2.http.Path("username") String username, @retrofit2.http.Body User user
  );

}
