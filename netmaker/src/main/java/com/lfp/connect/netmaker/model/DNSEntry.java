/*
 * Netmaker
 * API Usage  Most actions that can be performed via API can be performed via UI. We recommend managing your networks using the official netmaker-ui project. However, Netmaker can also be run without the UI, and all functions can be achieved via API calls. If your use case requires using Netmaker without the UI or you need to do some troubleshooting/advanced configuration, using the API directly may help.   Authentication  API calls must be authenticated via a header of the format -H “Authorization: Bearer <YOUR_SECRET_KEY>” There are two methods to obtain YOUR_SECRET_KEY: 1. Using the masterkey. By default, this value is “secret key,” but you should change this on your instance and keep it secure. This value can be set via env var at startup or in a config file (config/environments/< env >.yaml). See the [Netmaker](https://docs.netmaker.org/index.html) documentation for more details. 2. Using a JWT received for a node. This can be retrieved by calling the /api/nodes/<network>/authenticate endpoint, as documented below.
 *
 * OpenAPI spec version: 0.15.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lfp.connect.netmaker.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * DNSEntry - a DNS entry represented as struct
 */
@ApiModel(description = "DNSEntry - a DNS entry represented as struct")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2022-12-12T15:30:52.086-05:00")
public class DNSEntry {
  @SerializedName("address")
  private String address = null;

  @SerializedName("address6")
  private String address6 = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("network")
  private String network = null;

  public DNSEntry address(String address) {
    this.address = address;
    return this;
  }

   /**
   * Get address
   * @return address
  **/
  @ApiModelProperty(value = "")
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public DNSEntry address6(String address6) {
    this.address6 = address6;
    return this;
  }

   /**
   * Get address6
   * @return address6
  **/
  @ApiModelProperty(value = "")
  public String getAddress6() {
    return address6;
  }

  public void setAddress6(String address6) {
    this.address6 = address6;
  }

  public DNSEntry name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public DNSEntry network(String network) {
    this.network = network;
    return this;
  }

   /**
   * Get network
   * @return network
  **/
  @ApiModelProperty(value = "")
  public String getNetwork() {
    return network;
  }

  public void setNetwork(String network) {
    this.network = network;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DNSEntry dnSEntry = (DNSEntry) o;
    return Objects.equals(this.address, dnSEntry.address) &&
        Objects.equals(this.address6, dnSEntry.address6) &&
        Objects.equals(this.name, dnSEntry.name) &&
        Objects.equals(this.network, dnSEntry.network);
  }

  @Override
  public int hashCode() {
    return Objects.hash(address, address6, name, network);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DNSEntry {\n");
    
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    address6: ").append(toIndentedString(address6)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    network: ").append(toIndentedString(network)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

