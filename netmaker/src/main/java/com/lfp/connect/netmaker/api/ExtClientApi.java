package com.lfp.connect.netmaker.api;

import com.lfp.connect.netmaker.invoker.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.lfp.connect.netmaker.model.CustomExtClient;
import com.lfp.connect.netmaker.model.ExtClient;
import com.lfp.connect.netmaker.model.SuccessResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ExtClientApi {
  /**
   * Create an individual extclient.  Must have valid key and be unique.
   * 
   * @param network Network (required)
   * @param node Node ID (required)
   * @param customExtClient Custom ExtClient (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/extclients/{network}/{nodeid}")
  Call<Void> createExtClient(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("node") String node, @retrofit2.http.Body CustomExtClient customExtClient
  );

  /**
   * Delete an individual extclient.
   * 
   * @param clientid Client ID (required)
   * @param network Network (required)
   * @return Call&lt;SuccessResponse&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/extclients/{network}/{clientid}")
  Call<SuccessResponse> deleteExtClient(
    @retrofit2.http.Path("clientid") String clientid, @retrofit2.http.Path("network") String network
  );

  /**
   * A separate function to get all extclients, not just extclients for a particular network.
   * 
   * @param networks Networks (optional)
   * @return Call&lt;List&lt;ExtClient&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/extclients")
  Call<List<ExtClient>> getAllExtClients(
    @retrofit2.http.Body List<String> networks
  );

  /**
   * Get an individual extclient.
   * 
   * @param clientid Client ID (required)
   * @param network Network (required)
   * @return Call&lt;ExtClient&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/extclients/{network}/{clientid}")
  Call<ExtClient> getExtClient(
    @retrofit2.http.Path("clientid") String clientid, @retrofit2.http.Path("network") String network
  );

  /**
   * Get an individual extclient.
   * 
   * @param clientid Client ID (required)
   * @param network Network (required)
   * @return Call&lt;ExtClient&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/extclients/{network}/{clientid}/{type}")
  Call<ExtClient> getExtClientConf(
    @retrofit2.http.Path("clientid") String clientid, @retrofit2.http.Path("network") String network
  );

  /**
   * Get all extclients associated with network.
   * Gets all extclients associated with network, including pending extclients.
   * @param network Network (required)
   * @return Call&lt;List&lt;ExtClient&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/extclients/{network}")
  Call<List<ExtClient>> getNetworkExtClients(
    @retrofit2.http.Path("network") String network
  );

  /**
   * Update an individual extclient.
   * 
   * @param clientid Client ID (required)
   * @param network Network (required)
   * @param extClient ExtClient (optional)
   * @return Call&lt;ExtClient&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/extclients/{network}/{clientid}")
  Call<ExtClient> updateExtClient(
    @retrofit2.http.Path("clientid") String clientid, @retrofit2.http.Path("network") String network, @retrofit2.http.Body ExtClient extClient
  );

}
