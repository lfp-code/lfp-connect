package com.lfp.connect.netmaker.api;

import com.lfp.connect.netmaker.invoker.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.lfp.connect.netmaker.model.AuthParams;
import com.lfp.connect.netmaker.model.EgressGatewayRequest;
import com.lfp.connect.netmaker.model.Node;
import com.lfp.connect.netmaker.model.NodeGet;
import com.lfp.connect.netmaker.model.RelayRequest;
import com.lfp.connect.netmaker.model.SuccessResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface NodesApi {
  /**
   * Authenticate to make further API calls related to a network.
   * 
   * @param authParams AuthParams (optional)
   * @return Call&lt;SuccessResponse&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/nodes/adm/{network}/authenticate")
  Call<SuccessResponse> authenticate(
    @retrofit2.http.Body AuthParams authParams
  );

  /**
   * Create an egress gateway.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @param egressGatewayRequest Egress Gateway Request (optional)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/nodes/{network}/{nodeid}/creategateway")
  Call<Node> createEgressGateway(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid, @retrofit2.http.Body EgressGatewayRequest egressGatewayRequest
  );

  /**
   * Create an ingress gateway.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/nodes/{network}/{nodeid}/createingress")
  Call<Node> createIngressGateway(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid
  );

  /**
   * Create a node on a network.
   * 
   * @return Call&lt;NodeGet&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/nodes/{network}")
  Call<NodeGet> createNode();
    

  /**
   * Create a relay.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @param relayRequest Relay Request (optional)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/nodes/{network}/{nodeid}/createrelay")
  Call<Node> createRelay(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid, @retrofit2.http.Body RelayRequest relayRequest
  );

  /**
   * Delete an egress gateway.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/nodes/{network}/{nodeid}/deletegateway")
  Call<Node> deleteEgressGateway(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid
  );

  /**
   * Delete an ingress gateway.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/nodes/{network}/{nodeid}/deleteingress")
  Call<Node> deleteIngressGateway(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid
  );

  /**
   * Delete an individual node.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @param node Node (optional)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/nodes/{network}/{nodeid}")
  Call<Node> deleteNode(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid, @retrofit2.http.Body Node node
  );

  /**
   * Remove a relay.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/nodes/{network}/{nodeid}/deleterelay")
  Call<Node> deleteRelay(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid
  );

  /**
   * Get all nodes across all networks.
   * 
   * @return Call&lt;List&lt;Node&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/nodes")
  Call<List<Node>> getAllNodes();
    

  /**
   * Get the time that a network of nodes was last modified.
   * 
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/nodes/adm/{network}/lastmodified")
  Call<Void> getLastModified();
    

  /**
   * Gets all nodes associated with network including pending nodes.
   * 
   * @return Call&lt;List&lt;Node&gt;&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/nodes/{network}")
  Call<List<Node>> getNetworkNodes();
    

  /**
   * Get an individual node.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/nodes/{network}/{nodeid}")
  Call<Node> getNode(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid
  );

  /**
   * Handles OAuth login.
   * 
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/oauth/login")
  Call<Void> handleAuthLogin();
    

  /**
   * Takes a node out of pending state.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/nodes/{network}/{nodeid}/approve")
  Call<Node> uncordonNode(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid
  );

  /**
   * Update an individual node.
   * 
   * @param network Network (required)
   * @param nodeid Node ID (required)
   * @param node Node (optional)
   * @return Call&lt;Node&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @PUT("api/nodes/{network}/{nodeid}")
  Call<Node> updateNode(
    @retrofit2.http.Path("network") String network, @retrofit2.http.Path("nodeid") String nodeid, @retrofit2.http.Body Node node
  );

}
