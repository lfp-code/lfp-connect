/*
 * Netmaker
 * API Usage  Most actions that can be performed via API can be performed via UI. We recommend managing your networks using the official netmaker-ui project. However, Netmaker can also be run without the UI, and all functions can be achieved via API calls. If your use case requires using Netmaker without the UI or you need to do some troubleshooting/advanced configuration, using the API directly may help.   Authentication  API calls must be authenticated via a header of the format -H “Authorization: Bearer <YOUR_SECRET_KEY>” There are two methods to obtain YOUR_SECRET_KEY: 1. Using the masterkey. By default, this value is “secret key,” but you should change this on your instance and keep it secure. This value can be set via env var at startup or in a config file (config/environments/< env >.yaml). See the [Netmaker](https://docs.netmaker.org/index.html) documentation for more details. 2. Using a JWT received for a node. This can be retrieved by calling the /api/nodes/<network>/authenticate endpoint, as documented below.
 *
 * OpenAPI spec version: 0.15.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lfp.connect.netmaker.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * ServerConfig - server conf struct
 */
@ApiModel(description = "ServerConfig - server conf struct")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2022-12-12T15:30:52.086-05:00")
public class ServerConfig {
  @SerializedName("APIConnString")
  private String apIConnString = null;

  @SerializedName("APIHost")
  private String apIHost = null;

  @SerializedName("APIPort")
  private String apIPort = null;

  @SerializedName("AgentBackend")
  private String agentBackend = null;

  @SerializedName("AllowedOrigin")
  private String allowedOrigin = null;

  @SerializedName("AuthProvider")
  private String authProvider = null;

  @SerializedName("AzureTenant")
  private String azureTenant = null;

  @SerializedName("ClientID")
  private String clientID = null;

  @SerializedName("ClientMode")
  private String clientMode = null;

  @SerializedName("ClientSecret")
  private String clientSecret = null;

  @SerializedName("CoreDNSAddr")
  private String coreDNSAddr = null;

  @SerializedName("DNSKey")
  private String dnSKey = null;

  @SerializedName("DNSMode")
  private String dnSMode = null;

  @SerializedName("Database")
  private String database = null;

  @SerializedName("DefaultNodeLimit")
  private Integer defaultNodeLimit = null;

  @SerializedName("DisableRemoteIPCheck")
  private String disableRemoteIPCheck = null;

  @SerializedName("DisplayKeys")
  private String displayKeys = null;

  @SerializedName("FrontendURL")
  private String frontendURL = null;

  @SerializedName("HostNetwork")
  private String hostNetwork = null;

  @SerializedName("MQHOST")
  private String MQHOST = null;

  @SerializedName("MQPort")
  private String mqPort = null;

  @SerializedName("MQServerPort")
  private String mqServerPort = null;

  @SerializedName("ManageIPTables")
  private String manageIPTables = null;

  @SerializedName("MasterKey")
  private String masterKey = null;

  @SerializedName("MessageQueueBackend")
  private String messageQueueBackend = null;

  @SerializedName("NodeID")
  private String nodeID = null;

  @SerializedName("OIDCIssuer")
  private String oiDCIssuer = null;

  @SerializedName("Platform")
  private String platform = null;

  @SerializedName("PortForwardServices")
  private String portForwardServices = null;

  @SerializedName("PublicIPService")
  private String publicIPService = null;

  @SerializedName("RCE")
  private String RCE = null;

  @SerializedName("RestBackend")
  private String restBackend = null;

  @SerializedName("SQLConn")
  private String sqLConn = null;

  @SerializedName("Server")
  private String server = null;

  @SerializedName("ServerCheckinInterval")
  private Long serverCheckinInterval = null;

  @SerializedName("Telemetry")
  private String telemetry = null;

  @SerializedName("Verbosity")
  private Integer verbosity = null;

  @SerializedName("Version")
  private String version = null;

  public ServerConfig apIConnString(String apIConnString) {
    this.apIConnString = apIConnString;
    return this;
  }

   /**
   * Get apIConnString
   * @return apIConnString
  **/
  @ApiModelProperty(value = "")
  public String getApIConnString() {
    return apIConnString;
  }

  public void setApIConnString(String apIConnString) {
    this.apIConnString = apIConnString;
  }

  public ServerConfig apIHost(String apIHost) {
    this.apIHost = apIHost;
    return this;
  }

   /**
   * Get apIHost
   * @return apIHost
  **/
  @ApiModelProperty(value = "")
  public String getApIHost() {
    return apIHost;
  }

  public void setApIHost(String apIHost) {
    this.apIHost = apIHost;
  }

  public ServerConfig apIPort(String apIPort) {
    this.apIPort = apIPort;
    return this;
  }

   /**
   * Get apIPort
   * @return apIPort
  **/
  @ApiModelProperty(value = "")
  public String getApIPort() {
    return apIPort;
  }

  public void setApIPort(String apIPort) {
    this.apIPort = apIPort;
  }

  public ServerConfig agentBackend(String agentBackend) {
    this.agentBackend = agentBackend;
    return this;
  }

   /**
   * Get agentBackend
   * @return agentBackend
  **/
  @ApiModelProperty(value = "")
  public String getAgentBackend() {
    return agentBackend;
  }

  public void setAgentBackend(String agentBackend) {
    this.agentBackend = agentBackend;
  }

  public ServerConfig allowedOrigin(String allowedOrigin) {
    this.allowedOrigin = allowedOrigin;
    return this;
  }

   /**
   * Get allowedOrigin
   * @return allowedOrigin
  **/
  @ApiModelProperty(value = "")
  public String getAllowedOrigin() {
    return allowedOrigin;
  }

  public void setAllowedOrigin(String allowedOrigin) {
    this.allowedOrigin = allowedOrigin;
  }

  public ServerConfig authProvider(String authProvider) {
    this.authProvider = authProvider;
    return this;
  }

   /**
   * Get authProvider
   * @return authProvider
  **/
  @ApiModelProperty(value = "")
  public String getAuthProvider() {
    return authProvider;
  }

  public void setAuthProvider(String authProvider) {
    this.authProvider = authProvider;
  }

  public ServerConfig azureTenant(String azureTenant) {
    this.azureTenant = azureTenant;
    return this;
  }

   /**
   * Get azureTenant
   * @return azureTenant
  **/
  @ApiModelProperty(value = "")
  public String getAzureTenant() {
    return azureTenant;
  }

  public void setAzureTenant(String azureTenant) {
    this.azureTenant = azureTenant;
  }

  public ServerConfig clientID(String clientID) {
    this.clientID = clientID;
    return this;
  }

   /**
   * Get clientID
   * @return clientID
  **/
  @ApiModelProperty(value = "")
  public String getClientID() {
    return clientID;
  }

  public void setClientID(String clientID) {
    this.clientID = clientID;
  }

  public ServerConfig clientMode(String clientMode) {
    this.clientMode = clientMode;
    return this;
  }

   /**
   * Get clientMode
   * @return clientMode
  **/
  @ApiModelProperty(value = "")
  public String getClientMode() {
    return clientMode;
  }

  public void setClientMode(String clientMode) {
    this.clientMode = clientMode;
  }

  public ServerConfig clientSecret(String clientSecret) {
    this.clientSecret = clientSecret;
    return this;
  }

   /**
   * Get clientSecret
   * @return clientSecret
  **/
  @ApiModelProperty(value = "")
  public String getClientSecret() {
    return clientSecret;
  }

  public void setClientSecret(String clientSecret) {
    this.clientSecret = clientSecret;
  }

  public ServerConfig coreDNSAddr(String coreDNSAddr) {
    this.coreDNSAddr = coreDNSAddr;
    return this;
  }

   /**
   * Get coreDNSAddr
   * @return coreDNSAddr
  **/
  @ApiModelProperty(value = "")
  public String getCoreDNSAddr() {
    return coreDNSAddr;
  }

  public void setCoreDNSAddr(String coreDNSAddr) {
    this.coreDNSAddr = coreDNSAddr;
  }

  public ServerConfig dnSKey(String dnSKey) {
    this.dnSKey = dnSKey;
    return this;
  }

   /**
   * Get dnSKey
   * @return dnSKey
  **/
  @ApiModelProperty(value = "")
  public String getDnSKey() {
    return dnSKey;
  }

  public void setDnSKey(String dnSKey) {
    this.dnSKey = dnSKey;
  }

  public ServerConfig dnSMode(String dnSMode) {
    this.dnSMode = dnSMode;
    return this;
  }

   /**
   * Get dnSMode
   * @return dnSMode
  **/
  @ApiModelProperty(value = "")
  public String getDnSMode() {
    return dnSMode;
  }

  public void setDnSMode(String dnSMode) {
    this.dnSMode = dnSMode;
  }

  public ServerConfig database(String database) {
    this.database = database;
    return this;
  }

   /**
   * Get database
   * @return database
  **/
  @ApiModelProperty(value = "")
  public String getDatabase() {
    return database;
  }

  public void setDatabase(String database) {
    this.database = database;
  }

  public ServerConfig defaultNodeLimit(Integer defaultNodeLimit) {
    this.defaultNodeLimit = defaultNodeLimit;
    return this;
  }

   /**
   * Get defaultNodeLimit
   * @return defaultNodeLimit
  **/
  @ApiModelProperty(value = "")
  public Integer getDefaultNodeLimit() {
    return defaultNodeLimit;
  }

  public void setDefaultNodeLimit(Integer defaultNodeLimit) {
    this.defaultNodeLimit = defaultNodeLimit;
  }

  public ServerConfig disableRemoteIPCheck(String disableRemoteIPCheck) {
    this.disableRemoteIPCheck = disableRemoteIPCheck;
    return this;
  }

   /**
   * Get disableRemoteIPCheck
   * @return disableRemoteIPCheck
  **/
  @ApiModelProperty(value = "")
  public String getDisableRemoteIPCheck() {
    return disableRemoteIPCheck;
  }

  public void setDisableRemoteIPCheck(String disableRemoteIPCheck) {
    this.disableRemoteIPCheck = disableRemoteIPCheck;
  }

  public ServerConfig displayKeys(String displayKeys) {
    this.displayKeys = displayKeys;
    return this;
  }

   /**
   * Get displayKeys
   * @return displayKeys
  **/
  @ApiModelProperty(value = "")
  public String getDisplayKeys() {
    return displayKeys;
  }

  public void setDisplayKeys(String displayKeys) {
    this.displayKeys = displayKeys;
  }

  public ServerConfig frontendURL(String frontendURL) {
    this.frontendURL = frontendURL;
    return this;
  }

   /**
   * Get frontendURL
   * @return frontendURL
  **/
  @ApiModelProperty(value = "")
  public String getFrontendURL() {
    return frontendURL;
  }

  public void setFrontendURL(String frontendURL) {
    this.frontendURL = frontendURL;
  }

  public ServerConfig hostNetwork(String hostNetwork) {
    this.hostNetwork = hostNetwork;
    return this;
  }

   /**
   * Get hostNetwork
   * @return hostNetwork
  **/
  @ApiModelProperty(value = "")
  public String getHostNetwork() {
    return hostNetwork;
  }

  public void setHostNetwork(String hostNetwork) {
    this.hostNetwork = hostNetwork;
  }

  public ServerConfig MQHOST(String MQHOST) {
    this.MQHOST = MQHOST;
    return this;
  }

   /**
   * Get MQHOST
   * @return MQHOST
  **/
  @ApiModelProperty(value = "")
  public String getMQHOST() {
    return MQHOST;
  }

  public void setMQHOST(String MQHOST) {
    this.MQHOST = MQHOST;
  }

  public ServerConfig mqPort(String mqPort) {
    this.mqPort = mqPort;
    return this;
  }

   /**
   * Get mqPort
   * @return mqPort
  **/
  @ApiModelProperty(value = "")
  public String getMqPort() {
    return mqPort;
  }

  public void setMqPort(String mqPort) {
    this.mqPort = mqPort;
  }

  public ServerConfig mqServerPort(String mqServerPort) {
    this.mqServerPort = mqServerPort;
    return this;
  }

   /**
   * Get mqServerPort
   * @return mqServerPort
  **/
  @ApiModelProperty(value = "")
  public String getMqServerPort() {
    return mqServerPort;
  }

  public void setMqServerPort(String mqServerPort) {
    this.mqServerPort = mqServerPort;
  }

  public ServerConfig manageIPTables(String manageIPTables) {
    this.manageIPTables = manageIPTables;
    return this;
  }

   /**
   * Get manageIPTables
   * @return manageIPTables
  **/
  @ApiModelProperty(value = "")
  public String getManageIPTables() {
    return manageIPTables;
  }

  public void setManageIPTables(String manageIPTables) {
    this.manageIPTables = manageIPTables;
  }

  public ServerConfig masterKey(String masterKey) {
    this.masterKey = masterKey;
    return this;
  }

   /**
   * Get masterKey
   * @return masterKey
  **/
  @ApiModelProperty(value = "")
  public String getMasterKey() {
    return masterKey;
  }

  public void setMasterKey(String masterKey) {
    this.masterKey = masterKey;
  }

  public ServerConfig messageQueueBackend(String messageQueueBackend) {
    this.messageQueueBackend = messageQueueBackend;
    return this;
  }

   /**
   * Get messageQueueBackend
   * @return messageQueueBackend
  **/
  @ApiModelProperty(value = "")
  public String getMessageQueueBackend() {
    return messageQueueBackend;
  }

  public void setMessageQueueBackend(String messageQueueBackend) {
    this.messageQueueBackend = messageQueueBackend;
  }

  public ServerConfig nodeID(String nodeID) {
    this.nodeID = nodeID;
    return this;
  }

   /**
   * Get nodeID
   * @return nodeID
  **/
  @ApiModelProperty(value = "")
  public String getNodeID() {
    return nodeID;
  }

  public void setNodeID(String nodeID) {
    this.nodeID = nodeID;
  }

  public ServerConfig oiDCIssuer(String oiDCIssuer) {
    this.oiDCIssuer = oiDCIssuer;
    return this;
  }

   /**
   * Get oiDCIssuer
   * @return oiDCIssuer
  **/
  @ApiModelProperty(value = "")
  public String getOiDCIssuer() {
    return oiDCIssuer;
  }

  public void setOiDCIssuer(String oiDCIssuer) {
    this.oiDCIssuer = oiDCIssuer;
  }

  public ServerConfig platform(String platform) {
    this.platform = platform;
    return this;
  }

   /**
   * Get platform
   * @return platform
  **/
  @ApiModelProperty(value = "")
  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public ServerConfig portForwardServices(String portForwardServices) {
    this.portForwardServices = portForwardServices;
    return this;
  }

   /**
   * Get portForwardServices
   * @return portForwardServices
  **/
  @ApiModelProperty(value = "")
  public String getPortForwardServices() {
    return portForwardServices;
  }

  public void setPortForwardServices(String portForwardServices) {
    this.portForwardServices = portForwardServices;
  }

  public ServerConfig publicIPService(String publicIPService) {
    this.publicIPService = publicIPService;
    return this;
  }

   /**
   * Get publicIPService
   * @return publicIPService
  **/
  @ApiModelProperty(value = "")
  public String getPublicIPService() {
    return publicIPService;
  }

  public void setPublicIPService(String publicIPService) {
    this.publicIPService = publicIPService;
  }

  public ServerConfig RCE(String RCE) {
    this.RCE = RCE;
    return this;
  }

   /**
   * Get RCE
   * @return RCE
  **/
  @ApiModelProperty(value = "")
  public String getRCE() {
    return RCE;
  }

  public void setRCE(String RCE) {
    this.RCE = RCE;
  }

  public ServerConfig restBackend(String restBackend) {
    this.restBackend = restBackend;
    return this;
  }

   /**
   * Get restBackend
   * @return restBackend
  **/
  @ApiModelProperty(value = "")
  public String getRestBackend() {
    return restBackend;
  }

  public void setRestBackend(String restBackend) {
    this.restBackend = restBackend;
  }

  public ServerConfig sqLConn(String sqLConn) {
    this.sqLConn = sqLConn;
    return this;
  }

   /**
   * Get sqLConn
   * @return sqLConn
  **/
  @ApiModelProperty(value = "")
  public String getSqLConn() {
    return sqLConn;
  }

  public void setSqLConn(String sqLConn) {
    this.sqLConn = sqLConn;
  }

  public ServerConfig server(String server) {
    this.server = server;
    return this;
  }

   /**
   * Get server
   * @return server
  **/
  @ApiModelProperty(value = "")
  public String getServer() {
    return server;
  }

  public void setServer(String server) {
    this.server = server;
  }

  public ServerConfig serverCheckinInterval(Long serverCheckinInterval) {
    this.serverCheckinInterval = serverCheckinInterval;
    return this;
  }

   /**
   * Get serverCheckinInterval
   * @return serverCheckinInterval
  **/
  @ApiModelProperty(value = "")
  public Long getServerCheckinInterval() {
    return serverCheckinInterval;
  }

  public void setServerCheckinInterval(Long serverCheckinInterval) {
    this.serverCheckinInterval = serverCheckinInterval;
  }

  public ServerConfig telemetry(String telemetry) {
    this.telemetry = telemetry;
    return this;
  }

   /**
   * Get telemetry
   * @return telemetry
  **/
  @ApiModelProperty(value = "")
  public String getTelemetry() {
    return telemetry;
  }

  public void setTelemetry(String telemetry) {
    this.telemetry = telemetry;
  }

  public ServerConfig verbosity(Integer verbosity) {
    this.verbosity = verbosity;
    return this;
  }

   /**
   * Get verbosity
   * @return verbosity
  **/
  @ApiModelProperty(value = "")
  public Integer getVerbosity() {
    return verbosity;
  }

  public void setVerbosity(Integer verbosity) {
    this.verbosity = verbosity;
  }

  public ServerConfig version(String version) {
    this.version = version;
    return this;
  }

   /**
   * Get version
   * @return version
  **/
  @ApiModelProperty(value = "")
  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ServerConfig serverConfig = (ServerConfig) o;
    return Objects.equals(this.apIConnString, serverConfig.apIConnString) &&
        Objects.equals(this.apIHost, serverConfig.apIHost) &&
        Objects.equals(this.apIPort, serverConfig.apIPort) &&
        Objects.equals(this.agentBackend, serverConfig.agentBackend) &&
        Objects.equals(this.allowedOrigin, serverConfig.allowedOrigin) &&
        Objects.equals(this.authProvider, serverConfig.authProvider) &&
        Objects.equals(this.azureTenant, serverConfig.azureTenant) &&
        Objects.equals(this.clientID, serverConfig.clientID) &&
        Objects.equals(this.clientMode, serverConfig.clientMode) &&
        Objects.equals(this.clientSecret, serverConfig.clientSecret) &&
        Objects.equals(this.coreDNSAddr, serverConfig.coreDNSAddr) &&
        Objects.equals(this.dnSKey, serverConfig.dnSKey) &&
        Objects.equals(this.dnSMode, serverConfig.dnSMode) &&
        Objects.equals(this.database, serverConfig.database) &&
        Objects.equals(this.defaultNodeLimit, serverConfig.defaultNodeLimit) &&
        Objects.equals(this.disableRemoteIPCheck, serverConfig.disableRemoteIPCheck) &&
        Objects.equals(this.displayKeys, serverConfig.displayKeys) &&
        Objects.equals(this.frontendURL, serverConfig.frontendURL) &&
        Objects.equals(this.hostNetwork, serverConfig.hostNetwork) &&
        Objects.equals(this.MQHOST, serverConfig.MQHOST) &&
        Objects.equals(this.mqPort, serverConfig.mqPort) &&
        Objects.equals(this.mqServerPort, serverConfig.mqServerPort) &&
        Objects.equals(this.manageIPTables, serverConfig.manageIPTables) &&
        Objects.equals(this.masterKey, serverConfig.masterKey) &&
        Objects.equals(this.messageQueueBackend, serverConfig.messageQueueBackend) &&
        Objects.equals(this.nodeID, serverConfig.nodeID) &&
        Objects.equals(this.oiDCIssuer, serverConfig.oiDCIssuer) &&
        Objects.equals(this.platform, serverConfig.platform) &&
        Objects.equals(this.portForwardServices, serverConfig.portForwardServices) &&
        Objects.equals(this.publicIPService, serverConfig.publicIPService) &&
        Objects.equals(this.RCE, serverConfig.RCE) &&
        Objects.equals(this.restBackend, serverConfig.restBackend) &&
        Objects.equals(this.sqLConn, serverConfig.sqLConn) &&
        Objects.equals(this.server, serverConfig.server) &&
        Objects.equals(this.serverCheckinInterval, serverConfig.serverCheckinInterval) &&
        Objects.equals(this.telemetry, serverConfig.telemetry) &&
        Objects.equals(this.verbosity, serverConfig.verbosity) &&
        Objects.equals(this.version, serverConfig.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(apIConnString, apIHost, apIPort, agentBackend, allowedOrigin, authProvider, azureTenant, clientID, clientMode, clientSecret, coreDNSAddr, dnSKey, dnSMode, database, defaultNodeLimit, disableRemoteIPCheck, displayKeys, frontendURL, hostNetwork, MQHOST, mqPort, mqServerPort, manageIPTables, masterKey, messageQueueBackend, nodeID, oiDCIssuer, platform, portForwardServices, publicIPService, RCE, restBackend, sqLConn, server, serverCheckinInterval, telemetry, verbosity, version);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ServerConfig {\n");
    
    sb.append("    apIConnString: ").append(toIndentedString(apIConnString)).append("\n");
    sb.append("    apIHost: ").append(toIndentedString(apIHost)).append("\n");
    sb.append("    apIPort: ").append(toIndentedString(apIPort)).append("\n");
    sb.append("    agentBackend: ").append(toIndentedString(agentBackend)).append("\n");
    sb.append("    allowedOrigin: ").append(toIndentedString(allowedOrigin)).append("\n");
    sb.append("    authProvider: ").append(toIndentedString(authProvider)).append("\n");
    sb.append("    azureTenant: ").append(toIndentedString(azureTenant)).append("\n");
    sb.append("    clientID: ").append(toIndentedString(clientID)).append("\n");
    sb.append("    clientMode: ").append(toIndentedString(clientMode)).append("\n");
    sb.append("    clientSecret: ").append(toIndentedString(clientSecret)).append("\n");
    sb.append("    coreDNSAddr: ").append(toIndentedString(coreDNSAddr)).append("\n");
    sb.append("    dnSKey: ").append(toIndentedString(dnSKey)).append("\n");
    sb.append("    dnSMode: ").append(toIndentedString(dnSMode)).append("\n");
    sb.append("    database: ").append(toIndentedString(database)).append("\n");
    sb.append("    defaultNodeLimit: ").append(toIndentedString(defaultNodeLimit)).append("\n");
    sb.append("    disableRemoteIPCheck: ").append(toIndentedString(disableRemoteIPCheck)).append("\n");
    sb.append("    displayKeys: ").append(toIndentedString(displayKeys)).append("\n");
    sb.append("    frontendURL: ").append(toIndentedString(frontendURL)).append("\n");
    sb.append("    hostNetwork: ").append(toIndentedString(hostNetwork)).append("\n");
    sb.append("    MQHOST: ").append(toIndentedString(MQHOST)).append("\n");
    sb.append("    mqPort: ").append(toIndentedString(mqPort)).append("\n");
    sb.append("    mqServerPort: ").append(toIndentedString(mqServerPort)).append("\n");
    sb.append("    manageIPTables: ").append(toIndentedString(manageIPTables)).append("\n");
    sb.append("    masterKey: ").append(toIndentedString(masterKey)).append("\n");
    sb.append("    messageQueueBackend: ").append(toIndentedString(messageQueueBackend)).append("\n");
    sb.append("    nodeID: ").append(toIndentedString(nodeID)).append("\n");
    sb.append("    oiDCIssuer: ").append(toIndentedString(oiDCIssuer)).append("\n");
    sb.append("    platform: ").append(toIndentedString(platform)).append("\n");
    sb.append("    portForwardServices: ").append(toIndentedString(portForwardServices)).append("\n");
    sb.append("    publicIPService: ").append(toIndentedString(publicIPService)).append("\n");
    sb.append("    RCE: ").append(toIndentedString(RCE)).append("\n");
    sb.append("    restBackend: ").append(toIndentedString(restBackend)).append("\n");
    sb.append("    sqLConn: ").append(toIndentedString(sqLConn)).append("\n");
    sb.append("    server: ").append(toIndentedString(server)).append("\n");
    sb.append("    serverCheckinInterval: ").append(toIndentedString(serverCheckinInterval)).append("\n");
    sb.append("    telemetry: ").append(toIndentedString(telemetry)).append("\n");
    sb.append("    verbosity: ").append(toIndentedString(verbosity)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

