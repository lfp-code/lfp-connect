package com.lfp.connect.netmaker.api;

import com.lfp.connect.netmaker.invoker.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.lfp.connect.netmaker.model.RegisterRequest;
import com.lfp.connect.netmaker.model.ServerConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ServerApi {
  /**
   * Get the server configuration.
   * 
   * @return Call&lt;ServerConfig&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/server/getconfig")
  Call<ServerConfig> getConfig();
    

  /**
   * Get the server configuration.
   * 
   * @return Call&lt;ServerConfig&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("api/server/getserverinfo")
  Call<ServerConfig> getServerInfo();
    

  /**
   * Registers a client with the server and return the Certificate Authority and certificate.
   * 
   * @param registerRequest Register Request (optional)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("api/server/register")
  Call<Void> register(
    @retrofit2.http.Body RegisterRequest registerRequest
  );

  /**
   * Remove a network from the server.
   * 
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @DELETE("api/server/removenetwork/{network}")
  Call<Void> removeNetwork();
    

}
