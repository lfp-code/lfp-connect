/*
 * Netmaker
 * API Usage  Most actions that can be performed via API can be performed via UI. We recommend managing your networks using the official netmaker-ui project. However, Netmaker can also be run without the UI, and all functions can be achieved via API calls. If your use case requires using Netmaker without the UI or you need to do some troubleshooting/advanced configuration, using the API directly may help.   Authentication  API calls must be authenticated via a header of the format -H “Authorization: Bearer <YOUR_SECRET_KEY>” There are two methods to obtain YOUR_SECRET_KEY: 1. Using the masterkey. By default, this value is “secret key,” but you should change this on your instance and keep it secure. This value can be set via env var at startup or in a config file (config/environments/< env >.yaml). See the [Netmaker](https://docs.netmaker.org/index.html) documentation for more details. 2. Using a JWT received for a node. This can be retrieved by calling the /api/nodes/<network>/authenticate endpoint, as documented below.
 *
 * OpenAPI spec version: 0.15.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.lfp.connect.netmaker.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * RelayRequest - relay request struct
 */
@ApiModel(description = "RelayRequest - relay request struct")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2022-12-12T15:30:52.086-05:00")
public class RelayRequest {
  @SerializedName("netid")
  private String netid = null;

  @SerializedName("nodeid")
  private String nodeid = null;

  @SerializedName("relayaddrs")
  private List<String> relayaddrs = null;

  public RelayRequest netid(String netid) {
    this.netid = netid;
    return this;
  }

   /**
   * Get netid
   * @return netid
  **/
  @ApiModelProperty(value = "")
  public String getNetid() {
    return netid;
  }

  public void setNetid(String netid) {
    this.netid = netid;
  }

  public RelayRequest nodeid(String nodeid) {
    this.nodeid = nodeid;
    return this;
  }

   /**
   * Get nodeid
   * @return nodeid
  **/
  @ApiModelProperty(value = "")
  public String getNodeid() {
    return nodeid;
  }

  public void setNodeid(String nodeid) {
    this.nodeid = nodeid;
  }

  public RelayRequest relayaddrs(List<String> relayaddrs) {
    this.relayaddrs = relayaddrs;
    return this;
  }

  public RelayRequest addRelayaddrsItem(String relayaddrsItem) {
    if (this.relayaddrs == null) {
      this.relayaddrs = new ArrayList<String>();
    }
    this.relayaddrs.add(relayaddrsItem);
    return this;
  }

   /**
   * Get relayaddrs
   * @return relayaddrs
  **/
  @ApiModelProperty(value = "")
  public List<String> getRelayaddrs() {
    return relayaddrs;
  }

  public void setRelayaddrs(List<String> relayaddrs) {
    this.relayaddrs = relayaddrs;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelayRequest relayRequest = (RelayRequest) o;
    return Objects.equals(this.netid, relayRequest.netid) &&
        Objects.equals(this.nodeid, relayRequest.nodeid) &&
        Objects.equals(this.relayaddrs, relayRequest.relayaddrs);
  }

  @Override
  public int hashCode() {
    return Objects.hash(netid, nodeid, relayaddrs);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RelayRequest {\n");
    
    sb.append("    netid: ").append(toIndentedString(netid)).append("\n");
    sb.append("    nodeid: ").append(toIndentedString(nodeid)).append("\n");
    sb.append("    relayaddrs: ").append(toIndentedString(relayaddrs)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

