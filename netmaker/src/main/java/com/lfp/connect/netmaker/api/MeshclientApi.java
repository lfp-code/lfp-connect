package com.lfp.connect.netmaker.api;

import com.lfp.connect.netmaker.invoker.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface MeshclientApi {
  /**
   * Retrieve a file from the file server.
   * 
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @GET("meshclient/files/{filename}")
  Call<Void> fileServer();
    

}
