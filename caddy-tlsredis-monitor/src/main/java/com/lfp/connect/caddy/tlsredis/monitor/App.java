package com.lfp.connect.caddy.tlsredis.monitor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.gson.JsonElement;
import com.lfp.caddy.core.config.Storage;
import com.lfp.joe.serial.Serials;

public class App {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		var file = new File("temp/config.json");
		JsonElement je;
		try (var fis = new FileInputStream(file)) {
			je = Serials.Gsons.fromStream(fis, JsonElement.class);
		}
		var storageJe = Serials.Gsons.tryGet(je, "storage").get();
		var storage = Serials.Gsons.get().fromJson(storageJe, Storage.class);
		var outDir = MigratorService.get().apply(storage);
		System.out.println(outDir.getAbsolutePath());
	}

}
