
package com.lfp.connect.caddy.tlsredis.monitor.storage;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Info {

    @SerializedName("sans")
    @Expose
    private List<String> sans = null;
    @SerializedName("issuer_data")
    @Expose
    private IssuerData issuerData;

    public List<String> getSans() {
        return sans;
    }

    public void setSans(List<String> sans) {
        this.sans = sans;
    }

    public IssuerData getIssuerData() {
        return issuerData;
    }

    public void setIssuerData(IssuerData issuerData) {
        this.issuerData = issuerData;
    }

}
