package com.lfp.connect.caddy.tlsredis.monitor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;

import com.google.gson.JsonObject;
import com.lfp.caddy.core.config.Storage;
import com.lfp.joe.core.classpath.Instances;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Asserts;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.io.FileExt;
import com.lfp.joe.process.Procs;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.KeyGenerator;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

public class MigratorService implements ThrowingFunction<Storage, FileExt, IOException> {

	public static MigratorService get() {
		return Instances.get(MigratorService.class);
	}

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;

	private static final String MIGRATOR_EXEC_PATH = "/migrator";
	private static final MemoizedSupplier<File> MIGRATOR_EXEC_SUPPLIER = Utils.Functions.memoize(() -> {
		return getMigratorExec();
	}, null);

	public MigratorService() {
	}

	@Override
	public FileExt apply(Storage input) throws IOException {
		var config = getConfig(input);
		var outDir = Utils.Files.tempFile(THIS_CLASS, VERSION, Utils.Crypto.getSecureRandomString()).deleteAllOnScrap(true);
		outDir.mkdirs();
		var command = String.format("%s -config %s export redis %s", MIGRATOR_EXEC_SUPPLIER.get().getAbsolutePath(),
				config.getAbsolutePath(), outDir.getAbsolutePath());
		Procs.execute(command);
		return outDir;
	}

	private static File getConfig(Storage storage) throws IOException {
		var jo = new JsonObject();
		if (storage != null)
			jo.add("storage", Serials.Gsons.get().toJsonTree(storage));
		var dir = Utils.Files.tempFile(THIS_CLASS, VERSION, Utils.Crypto.hashMD5(jo.toString()).encodeHex());
		dir.mkdirs();
		var config = new File(dir, "config.json");
		Utils.Files.configureDirectoryLocked(dir, null, nil -> {
			try (var fos = new FileOutputStream(config)) {
				Serials.Gsons.toStream(jo, fos);
			}
		});
		return config;
	}

	private static File getMigratorExec() throws IOException {
		var exec = new File(MIGRATOR_EXEC_PATH);
		if (exec.exists() && exec.canExecute())
			return exec;
		var install = MachineConfig.isDeveloper();
		Asserts.isTrue(install, ctx -> ctx.exceptionArguments("migrator not installed:%s", MIGRATOR_EXEC_PATH));
		File installDir = new File(Utils.Files.tempDirectory(true),
				KeyGenerator.apply(THIS_CLASS, VERSION, "migrator"));
		installDir.mkdirs();
		Utils.Files.configureDirectoryLocked(installDir, (f, elapsed) -> {
			return Duration.ofDays(7).toMillis() > elapsed.toMillis();
		}, f -> {
			installMigrator(f);
		});
		return getMigratorExec(installDir);
	}

	private static File getMigratorExec(File directory) {
		var migratorExecDir = getMigratorExecDir(directory);
		var ext = MachineConfig.isWindows() ? ".exe" : "";
		var exec = new File(migratorExecDir, "migrator" + ext);
		if (exec.exists() && exec.canExecute())
			return exec;
		throw Asserts.createException("migrator install failed:%s", migratorExecDir.getAbsolutePath());
	}

	private static File getMigratorExecDir(File directory) {
		return new File(directory, "cmd/migrator");
	}

	private static void installMigrator(File directory) throws IOException {
		var cloneDir = new File(directory, "clone");
		Procs.execute(String.format("git clone https://github.com/masipcat/caddy-storage-migrator %s",
				cloneDir.getAbsolutePath()), directory);
		for (var file : cloneDir.listFiles())
			FileUtils.moveToDirectory(file, directory, true);
		cloneDir.delete();
		Procs.execute("go build", getMigratorExecDir(directory));
		getMigratorExec(directory);
	}

}
