package com.lfp.connect.undertow.retrofit;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Spliterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.BaseStream;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringEscapeUtils;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.handler.ThreadHttpHandler;
import com.lfp.joe.core.lot.AbstractLot;
import com.lfp.joe.core.process.executor.CoreTasks;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.retrofit.Services;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.function.Requires;
import com.lfp.joe.core.function.Throws.ThrowingFunction;

import at.favre.lib.bytes.Bytes;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.PathHandler;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormData.FormValue;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;
import io.undertow.util.AttachmentKey;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import okhttp3.ResponseBody;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class RetrofitHandler<X> extends PathHandler {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final AttachmentKey<Object> BODY_ATTACHMENT = AttachmentKey.create(Object.class);
	private static final AttachmentKey<Optional<FormData>> FORM_DATA_ATTACHMENT = AttachmentKey.create(Optional.class);
	private final Map<Thread, HttpServerExchange> httpServerExchangeThreadTracker = new ConcurrentHashMap<>();
	private final FormParserFactory formParserFactory = FormParserFactory.builder().build();
	private final Class<X> serviceType;
	private final X serviceHandler;
	private final Gson gson;
	private final Executor executor;

	public <S extends X> RetrofitHandler(Class<X> serviceType, S service) {
		this(serviceType, asFunction(service));
	}

	public <S extends X> RetrofitHandler(Class<X> serviceType, Function<RetrofitHandler<X>, S> serviceMapper) {
		this(serviceType, serviceMapper, Services.INSTANCE.gson(), null);
	}

	public <S extends X> RetrofitHandler(Class<X> serviceType, Function<RetrofitHandler<X>, S> serviceMapper, Gson gson,
			Executor executor) {
		this.serviceType = Objects.requireNonNull(serviceType);
		this.gson = Objects.requireNonNull(gson);
		this.executor = Optional.ofNullable(executor).orElse(CoreTasks.executor());
		this.serviceHandler = Objects.requireNonNull(serviceMapper).apply(this);
		StreamEx<Method> methodStream;
		{
			methodStream = Utils.Lots.stream(streamHierarchy(serviceHandler.getClass()))
					.map(RetrofitHandler::streamPublicNonStaticMethods).flatMap(v -> v);
			methodStream = methodStream.filter(v -> v.isDefault() || !Modifier.isAbstract(v.getModifiers()));
			methodStream = methodStream.distinct(RetrofitHandler::getMethodSignature);
		}
		EntryStream<Method, MethodRoute> methodToMethodRouteStream;
		{
			methodToMethodRouteStream = methodStream.mapToEntry(m -> parseMethodRoute(m).orElse(null));
			methodToMethodRouteStream = methodToMethodRouteStream.nonNullValues();
			Comparator<Entry<Method, MethodRoute>> sorter = (ent1, ent2) -> {
				Comparator<MethodRoute> methodRouteSorter = (nil1, nil2) -> 0;
				methodRouteSorter = methodRouteSorter.thenComparing(v -> v.getPath());
				methodRouteSorter = methodRouteSorter.thenComparing(v -> v.streamParameterParsers().count());
				return methodRouteSorter.compare(ent1.getValue(), ent2.getValue());
			};
			methodToMethodRouteStream = methodToMethodRouteStream.sorted(sorter);
		}
		for (var ent : methodToMethodRouteStream) {
			Method method = ent.getKey();
			MethodRoute methodRoute = ent.getValue();
			method.setAccessible(true);
			HttpHandler handler = toHandler(method, methodRoute);
			var path = methodRoute.getPath();
			if (methodRoute.isPathPrefix()) {
				if ("/".equals(path))
					this.addPrefixPath(path, handler);
				else {
					this.addExactPath(path, handler);
					this.addPrefixPath(path + "/", handler);
				}
			} else {
				if ("/".equals(path))
					this.addExactPath(path, handler);
				else {
					this.addExactPath(path, handler);
					this.addExactPath(path + "/", handler);
				}
			}
		}
	}

	protected RetrofitHandler(Class<X> serviceType) {
		this(serviceType, rh -> Requires.isInstanceOf(rh, serviceType), Services.INSTANCE.gson(), null);
	}

	public Optional<HttpServerExchange> getCurrentHttpServerExchange() {
		return Optional.ofNullable(this.httpServerExchangeThreadTracker.get(Thread.currentThread()));
	}

	public X getServiceHandler() {
		return serviceHandler;
	}

	@SuppressWarnings("unchecked")
	private StreamEx<Class<?>> streamHierarchy(Class<?> classType) {
		Spliterator<Class<?>> hierarchyIterator = new AbstractLot.Indexed<Class<?>>() {

			private Class<?> next = classType;

			@Override
			protected Class<?> computeNext(long index) {
				if (next == null)
					return end();
				Class<?> result = next;
				next = next.getSuperclass();
				if (Object.class == next)
					next = null;
				return result;
			}

		};
		List<Class<?>> list = Utils.Lots.stream(hierarchyIterator).distinct().toList();
		StreamEx<Class<?>> result = StreamEx.of(list);
		for (Class<?> ct : list)
			result = result.append(JavaCode.Reflections.streamInterfaces(ct));
		result = result.distinct();
		return result;
	}

	private Optional<MethodRoute> parseMethodRoute(Method serviceHandlerMethod) {
		String searchSignature = getMethodSignature(serviceHandlerMethod);
		StreamEx<Method> serviceTypeMethods = streamPublicNonStaticMethods(this.serviceType).filter(v -> {
			return searchSignature.equals(getMethodSignature(v));
		});
		return serviceTypeMethods.map(v -> {
			StreamEx<Annotation> annoStream = streamRetrofitAnnotations(v.getAnnotations());
			MethodRoute methodRoute = annoStream.map(anno -> parseMethodRoute(v, anno)).nonNull().findFirst()
					.orElse(null);
			return methodRoute;
		}).nonNull().findFirst();
	}

	private MethodRoute parseMethodRoute(Method method, Annotation annotation) {
		com.goebl.david.Request.Method httpMethod = parseHttpMethod(annotation);
		if (httpMethod == null)
			return null;
		Optional<String> valueOp = getValue(annotation);
		if (valueOp == null)
			return null;
		Entry<String, Boolean> pathEntry = normalizePath(valueOp.orElse(null));
		List<ThrowingFunction<HttpServerExchange, Object, IOException>> parameterParsers = Utils.Lots
				.stream(method.getParameters()).map(this::getParameterParser).toList();
		return MethodRoute.builder().method(method).httpMethod(httpMethod).path(pathEntry.getKey())
				.pathPrefix(pathEntry.getValue()).parameterParsers(parameterParsers).build();
	}

	private ThrowingFunction<HttpServerExchange, Object, IOException> getParameterParser(Parameter parameter) {
		{// path
			Path anno = parameter.getAnnotation(Path.class);
			if (anno != null)
				return hsx -> {
					return deserialize(parameter, anno.value(), hsx.getPathParameters());
				};
		}
		{// form (must use eager parsing upstream)
			Field anno = parameter.getAnnotation(Field.class);
			if (anno != null) {
				String name = Optional.ofNullable(Utils.Strings.trimToNull(anno.value())).orElse(parameter.getName());
				return hsx -> {
					Optional<FormData> formDataOp = UndertowUtils.loadAttachment(hsx, FORM_DATA_ATTACHMENT, () -> {
						var formData = hsx.getAttachment(FormDataParser.FORM_DATA);
						if (formData != null)
							return Optional.of(formData);
						FormDataParser parser = formParserFactory.createParser(hsx);
						if (parser == null)
							return Optional.empty();
						hsx.startBlocking();
						return Optional.ofNullable(Utils.Functions.catching(() -> parser.parseBlocking(), t -> null));
					});
					List<String> values;
					if (formDataOp.isEmpty())
						values = null;
					else
						values = Utils.Lots.stream(formDataOp.get().get(name)).nonNull().map(FormValue::getValue)
								.toList();
					return deserialize(parameter, values);
				};
			}
		}
		{// body
			Body anno = parameter.getAnnotation(Body.class);
			if (anno != null) {
				return hsx -> {
					var errorRef = new AtomicReference<Exception>();
					Object bodyValue = UndertowUtils.loadAttachment(hsx, BODY_ATTACHMENT, () -> {
						hsx.startBlocking();
						try (InputStream is = hsx.getInputStream()) {
							if (Bytes.class.equals(parameter.getType()))
								return Bytes.from(is);
							else
								return Serials.Gsons.fromStream(this.gson, is, parameter.getType());
						} catch (Exception e) {
							errorRef.set(e);
							return null;
						}
					});
					if (errorRef.get() != null)
						throw Utils.Exceptions.as(errorRef.get(), IOException.class);
					return bodyValue;
				};
			}
		}
		// assume query as a last resort
		Query query = parameter.getAnnotation(Query.class);
		String name = query == null ? null : query.value();
		if (Utils.Strings.isBlank(name))
			name = parameter.getName();
		if (Utils.Strings.isBlank(name))
			return hsx -> null;
		final String nameF = name;
		return hsx -> {
			return deserialize(parameter, nameF, hsx.getQueryParameters());
		};
	}

	private Bytes serialize(Object result) {
		if (result instanceof BaseStream) {
			var streamOp = Utils.Lots.tryStream(result);
			if (streamOp.isPresent())
				result = streamOp.get().toList();
		}
		return Serials.Gsons.toBytes(gson, result);
	}

	private Object deserialize(Parameter parameter, String name, Map<String, Deque<String>> parameters) {
		List<String> values;
		if (parameters == null)
			values = null;
		else
			values = Utils.Lots.stream(parameters.get(name)).toList();
		return deserialize(parameter, values);
	}

	@SuppressWarnings("deprecation")
	private Object deserialize(Parameter parameter, List<String> values) {
		var type = parameter == null ? null : parameter.getParameterizedType();
		if (values == null)
			return deserialize(this.gson, type, null);
		var classType = Utils.Types.toClass(type);
		boolean firstOnly;
		if (classType == null)
			firstOnly = true;
		else if (Iterable.class.isAssignableFrom(classType))
			firstOnly = false;
		else if (Stream.class.isAssignableFrom(classType))
			firstOnly = false;
		else if (classType.isArray())
			firstOnly = false;
		else
			firstOnly = true;
		if (firstOnly) {
			var value = Utils.Lots.stream(values).nonNull().findFirst().orElse(null);
			return deserialize(this.gson, type, value);
		}
		var value = Utils.Lots.stream(values).map(v -> {
			if (v == null)
				return null;
			v = StringEscapeUtils.escapeEcmaScript(v);
			return String.format("\"%s\"", v);
		}).joining(",");
		value = String.format("[%s]", value);
		return deserialize(this.gson, type, value);
	}

	private static Object deserialize(Gson gson, Type type, String value) {
		if (String.class.isAssignableFrom(Utils.Types.toClass(type)))
			return value;
		Object result;
		try {
			result = gson.fromJson(value, type);
		} catch (JsonParseException e) {
			if (value == null)
				throw e;
			var jsonElement = Serials.Gsons.tryCreateJsonPrimitive(value).orElse(null);
			if (jsonElement == null)
				throw e;
			try {
				return gson.fromJson(jsonElement, type);
			} catch (Exception nil) {
				// suppress
			}
			throw e;
		}
		return result;
	}

	private HttpHandler toHandler(Method callMethod, MethodRoute methodRoute) {
		HttpHandler handler = hsx -> handle(callMethod, methodRoute, hsx);
		return new ThreadHttpHandler(handler, executor);
	}

	protected void handle(Method callMethod, MethodRoute methodRoute, HttpServerExchange hsx) throws Exception {
		List<Object> params = new ArrayList<>();
		{// get params or primitive defaults
			int index = -1;
			for (ThrowingFunction<HttpServerExchange, Object, IOException> pp : methodRoute.streamParameterParsers()) {
				index++;
				Object param = pp.apply(hsx);
				if (param == null)
					param = Utils.Types.getPrimitiveDefault(callMethod.getParameterTypes()[index]).orElse(null);
				params.add(param);
			}
		}
		Thread thread = Thread.currentThread();
		Object result;
		try {
			httpServerExchangeThreadTracker.put(thread, hsx);
			result = callMethod.invoke(serviceHandler, params.stream().toArray(Object[]::new));
		} catch (Throwable t) {
			logger.error("error invoking method:{} requestURI:{}", callMethod, UndertowUtils.getRequestURI(hsx), t);
			Bytes errorBytes = Serials.Gsons.toBytes(gson, t);
			writeResponse(StatusCodes.INTERNAL_SERVER_ERROR, errorBytes, hsx);
			return;
		} finally {
			httpServerExchangeThreadTracker.remove(thread);
		}
		if (result instanceof Call) {
			Response<?> response = ((Call<?>) result).execute();
			if (!response.isSuccessful()) {
				ResponseBody errorBody = response.errorBody();
				Bytes errorBytes;
				if (errorBody == null)
					errorBytes = Bytes.empty();
				else
					try (InputStream is = errorBody.byteStream()) {
						errorBytes = Bytes.from(is);
					}
				logger.error("error invoking method:{} requestURI:{} errorBody:{}", callMethod,
						UndertowUtils.getRequestURI(hsx), errorBytes.encodeUtf8());
				writeResponse(StatusCodes.INTERNAL_SERVER_ERROR, errorBytes, hsx);
				return;
			} else
				result = response.body();
		}
		Bytes resultBytes = serialize(result);
		if (resultBytes.isEmpty())
			writeResponse(StatusCodes.NO_CONTENT, resultBytes, hsx);
		else
			writeResponse(StatusCodes.OK, resultBytes, hsx);
	}

	private static void writeResponse(int statusCode, Bytes bytes, HttpServerExchange exchange) throws IOException {
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json; charset=utf-8");
		exchange.getResponseHeaders().put(Headers.CONTENT_LENGTH, bytes.length());
		exchange.startBlocking();
		try (java.io.OutputStream os = exchange.getOutputStream(); InputStream is = bytes.inputStream()) {
			Utils.Bits.copy(is, os);
		}
	}

	private static Entry<String, Boolean> normalizePath(String path) {
		boolean pathPrefix;
		if (Utils.Strings.endsWithIgnoreCase(path, "*")) {
			pathPrefix = true;
			path = path.substring(0, path.length() - 1);
		} else
			pathPrefix = false;
		if (Utils.Strings.isBlank(path))
			path = "/";
		return Utils.Lots.entry(path, pathPrefix);
	}

	private static Optional<String> getValue(Annotation annotation) {
		Method method = streamPublicNonStaticMethods(annotation.getClass()).filter(m -> "value".equals(m.getName()))
				.filter(m -> m.getParameterCount() == 0).findFirst().orElse(null);
		if (method == null)
			return null;
		return Optional.ofNullable(Utils.Functions.unchecked(() -> (String) method.invoke(annotation)));
	}

	private static com.goebl.david.Request.Method parseHttpMethod(Annotation v) {
		for (com.goebl.david.Request.Method value : com.goebl.david.Request.Method.values())
			if (Utils.Strings.equalsIgnoreCase(value.name(), v.annotationType().getSimpleName()))
				return value;
		return null;
	}

	private static StreamEx<Annotation> streamRetrofitAnnotations(Annotation[] annotations) {
		return Utils.Lots.stream(annotations).nonNull().filter(v -> {
			Class<? extends Annotation> annotationType = v.annotationType();
			return annotationType.getPackage().getName().startsWith("retrofit");
		});
	}

	@SuppressWarnings("unchecked")
	private static StreamEx<Method> streamPublicNonStaticMethods(Class<?> classType) {
		StreamEx<Method> methodStream = JavaCode.Reflections.streamMethods(classType, true,
				m -> !Modifier.isStatic(m.getModifiers()), m -> Modifier.isPublic(m.getModifiers()));
		return methodStream;
	}

	private static String getMethodSignature(Method m) {
		List<Object> parts = new ArrayList<>();
		parts.add(m.getName());
		parts.add(m.getParameterCount());
		for (Class<?> pt : m.getParameterTypes()) {
			pt = Utils.Types.wrapperToPrimitive(pt);
			parts.add(".pt." + pt.getName());
		}
		return Utils.Crypto.hashMD5(parts.stream().toArray(Object[]::new)).encodeHex();
	}

	private static <XX, SS extends XX> Function<RetrofitHandler<XX>, SS> asFunction(SS service) {
		return rh -> service;
	}

	public static void main(String[] args) throws Exception {
		var deserialize = deserialize(Serials.Gsons.get(), Date.class, "9/13/87");
		System.out.println(deserialize);
	}

}