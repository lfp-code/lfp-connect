package com.lfp.spring.run;

import java.net.InetSocketAddress;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.server.WebServer;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;
import com.lfp.spring.run.config.SpringConfig;

public class Springs {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final List<String> SERVER_ADDRESS_PROPERTY_NAMES = List.of("server.address", "local.server.address");
	private static final List<String> SERVER_PORT_PROPERTY_NAMES = List.of("server.port", "local.server.port");

	public static SpringApplication createApplication(Class<?>... primarySources) {
		return createApplication(Configs.get(SpringConfig.class).defaultAddress(), primarySources);
	}

	public static SpringApplication createApplication(InetSocketAddress listenAddress, Class<?>... primarySources) {
		Class<?> callingClass = CoreReflections.getCallingClass(THIS_CLASS);
		var primarySourcesList = Utils.Lots.stream(primarySources).nonNull().distinct().toList();
		for (int i = 0; primarySourcesList.isEmpty(); i++) {
			if (THIS_CLASS.equals(callingClass))
				continue;
			String name = callingClass.getName();
			name = Utils.Strings.substringBefore(name, "$");
			if (Utils.Strings.equals(THIS_CLASS.getName(), name))
				continue;
			primarySourcesList.add(callingClass);
		}
		Map<String, Object> defaultProperties = new LinkedHashMap<>();
		{
			SERVER_ADDRESS_PROPERTY_NAMES.forEach(v -> defaultProperties.put(v, listenAddress.getHostString()));
			SERVER_PORT_PROPERTY_NAMES.forEach(v -> defaultProperties.put(v, listenAddress.getPort()));
		}
		SpringApplication app = new SpringApplication(primarySourcesList.toArray(Class<?>[]::new));
		app.setDefaultProperties(defaultProperties);
		return app;
	}

	public static InetSocketAddress getAddress(ApplicationContext applicationContext) {
		Objects.requireNonNull(applicationContext);
		var environment = applicationContext.getBean(Environment.class);
		var ports = Utils.Lots.stream(applicationContext.getBeansOfType(WebServer.class)).values()
				.map(WebServer::getPort).filter(v -> v > 0).limit(2).toSet();
		if (ports.size() != 1)
			ports = Utils.Lots.stream(SERVER_PORT_PROPERTY_NAMES).map(v -> environment.getProperty(v, Integer.class))
					.nonNull().toSet();
		Validate.isTrue(ports.size() == 1, "unable to find port:%s", ports);
		var port = ports.iterator().next();
		String serverAddress = Utils.Lots.stream(SERVER_ADDRESS_PROPERTY_NAMES)
				.map(v -> environment.getProperty(v, String.class)).mapPartial(Utils.Strings::trimToNullOptional)
				.findFirst().orElse("127.0.0.1");
		InetSocketAddress address = new InetSocketAddress(serverAddress, port);
		return address;
	}

}
