package com.lfp.spring.run.config;

import java.net.InetSocketAddress;

import org.aeonbits.owner.Config;

import com.lfp.joe.properties.converter.InetSocketAddressConverter;

public interface SpringConfig extends Config {

	@DefaultValue("0.0.0.0:8080")
	@ConverterClass(InetSocketAddressConverter.class)
	InetSocketAddress defaultAddress();
}
