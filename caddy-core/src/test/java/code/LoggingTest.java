package code;

import com.lfp.caddy.core.CaddyConfig;
import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.joe.serial.Serials;

import ch.qos.logback.classic.Level;

public class LoggingTest {

	public static void main(String[] args) {
		var cfg = CaddyConfig.builder().build();
		cfg = CaddyConfigs.withLoggingLevel(cfg, Level.WARN);
		System.out.println(Serials.Gsons.getPretty().toJson(cfg));
	}
}
