package code;

import java.io.File;

import com.lfp.caddy.core.CaddyConfig;
import com.lfp.joe.beans.joda.BeanConverter;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.utils.Utils;

public class GenerateBuilders {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) {
		String path = "src/main/java/com/lfp/caddy/core/config";
		File folder = new File(path);
		var stream = Utils.Lots.stream(folder.listFiles());
		System.out.println(BeanConverter.get().apply(stream));
		JodaBeans.updateCode(CaddyConfig.class);
	}

}
