package code;

import java.io.File;

import com.lfp.caddy.core.CaddyConfigs;
import com.lfp.caddy.core.config.Issuer;
import com.lfp.joe.serial.Serials;

public class SSLTest {

	public static void main(String[] args) {
		var config = CaddyConfigs.getDefault(new File("C:\\Users\\reggi\\Apps\\caddy\\caddy.exe"));
		var issuer = Issuer.builder().module("zerossl").email("client.support@iplasso.com").build();
		config = CaddyConfigs.withTlsIssuer(config, issuer);
		System.out.println(Serials.Gsons.getPretty().toJson(config));
	}
}
