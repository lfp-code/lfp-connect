package com.lfp.caddy.core;

import com.lfp.joe.threads.Threads;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.time.Duration;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;
import org.joda.beans.MetaProperty;

import com.goebl.david.Request;
import com.goebl.david.Response;
import com.google.common.net.MediaType;
import com.lfp.caddy.core.config.Admin;
import com.lfp.caddy.core.config.Apps;
import com.lfp.caddy.core.config.AutomaticHttps;
import com.lfp.caddy.core.config.Automation;
import com.lfp.caddy.core.config.Default;
import com.lfp.caddy.core.config.Handle;
import com.lfp.caddy.core.config.HandleLayer4;
import com.lfp.caddy.core.config.Http;
import com.lfp.caddy.core.config.Issuer;
import com.lfp.caddy.core.config.LoadBalancing;
import com.lfp.caddy.core.config.Logging;
import com.lfp.caddy.core.config.Logs;
import com.lfp.caddy.core.config.MatchHttp;
import com.lfp.caddy.core.config.Policy;
import com.lfp.caddy.core.config.RequestHeaders;
import com.lfp.caddy.core.config.Route;
import com.lfp.caddy.core.config.SelectionPolicy;
import com.lfp.caddy.core.config.Server;
import com.lfp.caddy.core.config.Storage;
import com.lfp.caddy.core.config.Tls;
import com.lfp.caddy.core.config.TlsTransport;
import com.lfp.caddy.core.config.Transport;
import com.lfp.caddy.core.config.Upstream;
import com.lfp.caddy.core.config.UpstreamLayer4;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Throws.ThrowingBiFunction;
import com.lfp.joe.net.dns.DNSs;
import com.lfp.joe.net.http.client.HttpClients;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.socket.socks.Sockets;
import com.lfp.joe.serial.Serials;

import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import ch.qos.logback.classic.Level;
import io.mikael.urlbuilder.UrlBuilder;

public class CaddyConfigs {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final String LOCALHOST_IP = "127.0.0.1";
	private static final Duration DEFAULT_POLL_DURATION = Duration.ofSeconds(1);

	public static CaddyConfig getDefault(File caddyExec) {
		return getDefault(caddyExec, null);
	}

	public static CaddyConfig getDefault(File caddyExec, Integer adminPort) {
		Objects.requireNonNull(caddyExec);
		Validate.isTrue(caddyExec.exists(), "invalid caddy exec:%s", caddyExec.getAbsolutePath());
		var caddyConfig = CaddyConfig.builder().build();
		if (MachineConfig.isDeveloper())
			caddyConfig = withLoggingLevel(caddyConfig, Level.DEBUG);
		if (adminPort == null)
			caddyConfig = withAdminDisabled(caddyConfig);
		else
			caddyConfig = caddyConfig.toBuilder().admin(Admin.builder().listen(":" + adminPort).disabled(false).build())
					.build();
		File storageDir = new File(caddyExec.getParentFile(),
				Utils.Lots.stream("storage", VERSION, Utils.Files.tempDirectory().getName()).joining(File.separator));
		caddyConfig = withStorageFileSystem(caddyConfig, storageDir);
		return caddyConfig;
	}

	public static String toAddress(InetSocketAddress address) {
		Objects.requireNonNull(address);
		return String.format("%s:%s", address.getHostString(), address.getPort());
	}

	public static Handle toReverseProxyHandler(InetSocketAddress... addresses) {
		var uris = Utils.Lots.stream(addresses).nonNull().map(v -> {
			return URI.create(String.format("http://%s:%s", v.getHostString(), v.getPort()));
		});
		return toReverseProxyHandler(uris, null, null, null);
	}

	public static Handle toReverseProxyHandler(Iterable<URI> upstreamURIs) {
		return toReverseProxyHandler(upstreamURIs, null, null, null);
	}

	public static Handle toReverseProxyHandler(Iterable<URI> upstreamURIs, HeaderMap addHeaders, HeaderMap setHeaders,
			Iterable<String> deleteHeaders) {
		var uris = Utils.Lots.stream(upstreamURIs).nonNull().distinct().toList();
		var foundSecure = Utils.Lots.stream(uris).filter(URIs::isSecure).findFirst().isPresent();
		var foundInsecure = Utils.Lots.stream(uris).filter(Predicate.not(URIs::isSecure)).findFirst().isPresent();
		var foundNonIPAddressHost = Utils.Lots.stream(uris).filter(v -> !IPs.isValidIpAddress(v.getHost())).findFirst()
				.isPresent();
		Validate.isTrue(!foundSecure || !foundInsecure, "uri transport mismatch:%s", uris);
		var handleB = Handle.builder().handler("reverse_proxy");
		if (foundSecure) {
			var transport = Transport.builder().protocol("http").tls(TlsTransport.builder().build()).build();
			handleB.transport(transport);
		}
		com.lfp.caddy.core.config.Headers headers;
		{
			var meta = RequestHeaders.meta();
			Map<MetaProperty<?>, HeaderMap> headerSetupMap = new LinkedHashMap<>();
			headerSetupMap.put(meta.add(), HeaderMap.of(addHeaders));
			headerSetupMap.put(meta.set(), HeaderMap.of(setHeaders));
			headerSetupMap.put(meta.delete(),
					HeaderMap.of(Utils.Lots.stream(deleteHeaders).mapToEntry(v -> v, v -> "#")));
			if (foundNonIPAddressHost) {
				boolean hostHeaderExists = false;
				for (var ent : headerSetupMap.entrySet()) {
					if (ent.getValue().contains(Headers.HOST)) {
						hostHeaderExists = true;
						break;
					}
				}
				if (!hostHeaderExists)
					headerSetupMap.compute(meta.set(), (nil, hm) -> {
						return hm.with(Headers.HOST, "{http.reverse_proxy.upstream.host}");
					});
			}
			var notEmpty = Utils.Lots.stream(headerSetupMap).filterValues(v -> !v.isEmpty()).findFirst().isPresent();
			if (notEmpty) {
				var requestHeadersB = RequestHeaders.builder();
				for (var ent : headerSetupMap.entrySet()) {
					var mp = ent.getKey();
					var headerMap = ent.getValue();
					if (headerMap.isEmpty())
						continue;
					if (Map.class.isAssignableFrom(mp.propertyType()))
						requestHeadersB.set(mp, headerMap.map());
					else if (List.class.isAssignableFrom(mp.propertyType()))
						requestHeadersB.set(mp, headerMap.stream().keys().toList());
					else
						throw new IllegalStateException("unable to map header map to metaProperty:" + mp);
				}
				headers = com.lfp.caddy.core.config.Headers.builder().request(requestHeadersB.build()).build();
			} else
				headers = null;
		}
		handleB.headers(headers);
		var upstreams = Utils.Lots.stream(uris).map(v -> {
			String dial = String.format("%s:%s", v.getHost(), URIs.getPort(v));
			return Upstream.builder().dial(dial).build();
		}).toList();
		if (!upstreams.isEmpty())
			handleB.upstreams(upstreams);
		var loadBalancing = LoadBalancing.builder()
				.selectionPolicy(SelectionPolicy.builder().policy("least_conn").build()).build();
		handleB.loadBalancing(loadBalancing);
		return handleB.build();
	}

	public static HandleLayer4 toProxyHandleLayer4(InetSocketAddress address) {
		return HandleLayer4.builder().handler("proxy")
				.upstreams(UpstreamLayer4.builder().dial(toAddress(address)).build()).build();
	}

	public static CaddyConfig withAdminDisabled(CaddyConfig caddyConfig) {
		var def = Admin.builder().disabled(true).build();
		caddyConfig = JodaBeans.set(caddyConfig, def, CaddyConfig.meta().admin());
		return caddyConfig;
	}

	public static CaddyConfig withLoggingLevel(CaddyConfig caddyConfig, Level level) {
		var def = Default.builder().level(level.levelStr.toUpperCase()).build();
		caddyConfig = JodaBeans.set(caddyConfig, def, CaddyConfig.meta().logging(), Logging.meta().logs(),
				Logs.meta()._default());
		return caddyConfig;
	}

	public static CaddyConfig withStorageFileSystem(CaddyConfig caddyConfig, File folder) {
		Objects.requireNonNull(folder);
		folder.mkdirs();
		Validate.isTrue(folder.exists() && folder.isDirectory(), "invalid storage location:%s",
				folder.getAbsolutePath());
		var def = Storage.builder().module("file_system").root(folder.getAbsolutePath()).build();
		caddyConfig = JodaBeans.set(caddyConfig, def, CaddyConfig.meta().storage());
		return caddyConfig;
	}

	public static CaddyConfig withTlsIssuerInternal(CaddyConfig caddyConfig) {
		Policy policy = Policy.builder().keyType("rsa4096")
				.subjects("localhost", DNSs.toWildcardDNSHostname(IPs.getLocalIPAddress()))
				.issuers(Issuer.builder().module("internal").build()).build();
		caddyConfig = JodaBeans.set(caddyConfig, List.of(policy), CaddyConfig.meta().apps(), Apps.meta().tls(),
				Tls.meta().automation(), Automation.meta().policies());
		return caddyConfig;
	}

	public static CaddyConfig withTlsIssuer(CaddyConfig caddyConfig, Issuer issuer) {
		Policy policy = Policy.builder().issuers(issuer).build();
		caddyConfig = JodaBeans.set(caddyConfig, List.of(policy), CaddyConfig.meta().apps(), Apps.meta().tls(),
				Tls.meta().automation(), Automation.meta().policies());
		return caddyConfig;
	}

	public static Future<CaddyConfig> adminWait(int port) {
		return adminWait(getAdminURI(port));
	}

	public static Future<CaddyConfig> adminWait(URI uri) {
		Objects.requireNonNull(uri);
		return com.lfp.joe.threads.Threads.Pools.centralPool().submit(() -> {
			var request = HttpClients.getDefault().get(UrlBuilder.fromUri(uri).withPath("/config").toUri().toString());
			var responseBody = httpExecute(request, null, Duration.ofNanos(Long.MAX_VALUE), (i, response) -> {
				response.ensureSuccess();
				Validate.isTrue(response.getBody().length() > 0, "empty response");
				return true;
			});
			return Serials.Gsons.fromBytes(responseBody, CaddyConfig.class);
		});
	}

	public static void adminLoad(int port, CaddyConfig caddyConfig) throws IOException {
		adminLoad(getAdminURI(port), caddyConfig);
	}

	public static void adminLoad(URI uri, CaddyConfig caddyConfig) throws IOException {
		uri = UrlBuilder.fromUri(uri).withPath("/load").toUri();
		String version = createVersion();
		int port = Sockets.allocatePort();
		Server versionServer = getVersionServer(version, port);
		var servers = Optional.ofNullable(caddyConfig).map(CaddyConfig::getApps).map(Apps::getHttp)
				.map(Http::getServers).orElse(null);
		servers = Utils.Lots.stream(servers).append(String.format("server_%s", version), versionServer).toMap();
		caddyConfig = JodaBeans.set(caddyConfig, servers, CaddyConfig.meta().apps(), Apps.meta().http(),
				Http.meta().servers());
		if (MachineConfig.isDeveloper())
			logger.info("caddy config load. uri:{} body:{}", uri, Serials.Gsons.get().toJson(caddyConfig));
		{// upload config json
			var request = HttpClients.getDefault().post(uri.toString()).body(Serials.Gsons.toBytes(caddyConfig).array())
					.header(Headers.CONTENT_TYPE, MediaType.JSON_UTF_8);
			httpExecute(request, null, Duration.ofSeconds(5), (i, response) -> {
				if (i > 0)
					logger.info("retrying caddy load. attempt:{} uri:{}", i + 1, request.getUri());
				response.ensureSuccess();
				return true;
			});
		}
		{// wait for version
			var versionServerURI = getVersionServerURI(version, port);
			var request = HttpClients.getDefault().get(versionServerURI.toString());
			httpExecute(request, null, Duration.ofSeconds(15), (i, response) -> {
				if (i > 0)
					logger.info("awaiting caddy load complete. attempt:{} uri:{}", i + 1, request.getUri());
				response.ensureSuccess();
				var result = version.equals(response.getBody().encodeUtf8());
				return result;
			});
		}

	}

	private static Bytes httpExecute(Request request, Duration pollDuration, Duration attemptDuration,
			ThrowingBiFunction<Integer, Response<Bytes>, Boolean, IOException> responseFilter) throws IOException {
		long quitAt = attemptDuration == null ? 0 : System.currentTimeMillis() + attemptDuration.toMillis();
		Exception error = null;
		for (int i = 0; !Thread.currentThread().isInterrupted()
				&& (i == 0 || System.currentTimeMillis() < quitAt); i++) {
			if (i > 0)
				Utils.Functions.unchecked(() -> {
					var sleepMillis = Optional.ofNullable(pollDuration).orElse(DEFAULT_POLL_DURATION).toMillis();
					Thread.sleep(sleepMillis);
				});
			try {
				var response = httpExecute(request);
				if (Boolean.TRUE.equals(responseFilter.apply(i, response)))
					return response.getBody();
				error = new IOException(response.getBody().encodeUtf8());
			} catch (Exception e) {
				error = e;
			}
		}
		throw Utils.Exceptions.as(error, IOException.class);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Response<Bytes> httpExecute(Request request) throws IOException {
		var response = request.asStream();
		Bytes body;
		try (var is = HttpClients.getResponseBody(response)) {
			body = Utils.Bits.from(is);
		}
		if (body == null)
			body = Utils.Bits.empty();
		HttpClients.setBody(response, body);
		return (Response) response;
	}

	private static URI getAdminURI(int port) {
		return URI.create(String.format("http://%s:%s", LOCALHOST_IP, port));
	}

	private static String createVersion() {
		String result = String.format("%s_%s", new Date().getTime(), Utils.Crypto.getSecureRandomString());
		return result;
	}

	private static URI getVersionServerURI(String version, int port) {
		String path = String.format("/_config_version_%s", version);
		var url = String.format("http://%s:%s%s", LOCALHOST_IP, port, path);
		return URIs.parse(url).orElse(null);
	}

	private static Server getVersionServer(String version, int port) {
		var versionServerURI = getVersionServerURI(version, port);
		var match = MatchHttp.builder().path(versionServerURI.getPath()).build();
		var handle = Handle.builder().handler("static_response").statusCode(200).body(version).build();
		var route = Route.builder().match(match).handle(handle).build();
		return Server.builder().automaticHttps(AutomaticHttps.builder().disable(true).build())
				.listen(String.format("%s:%s", versionServerURI.getHost(), URIs.getPort(versionServerURI)))
				.routes(route).build();
	}

	public static void main(String[] args) {
		var cc = withLoggingLevel(CaddyConfig.builder().build(), Level.WARN);
		System.out.println(Serials.Gsons.getPretty().toJson(cc));
	}

}
