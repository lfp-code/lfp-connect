package com.lfp.connect.undertow.handler;

import java.net.URI;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.utils.Utils;
import com.stijndewitt.undertow.cors.AllowAll;
import com.stijndewitt.undertow.cors.AllowMatching;
import com.stijndewitt.undertow.cors.Filter;
import com.stijndewitt.undertow.cors.Policy;
import com.stijndewitt.undertow.cors.Whitelist;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.spi.FilterReply;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class CorsFilter implements HttpHandler {
	private static final String PATH_PATTERN_TEMPLATE = "http(s)?://([^/]+)(:([^/]+))?(/([^/])+)?%s(/.*)?$";
	private static final MemoizedSupplier<Void> LOG_INIT = Utils.Functions.memoize(() -> {
		LogFilter.addFilter(ctx -> {
			String loggerName = ctx.getEvent().getLoggerName();
			if (!Filter.class.getName().equals(loggerName))
				return FilterReply.NEUTRAL;
			if (!ctx.getEvent().getLevel().isGreaterOrEqual(Level.INFO))
				return FilterReply.NEUTRAL;
			return FilterReply.DENY;
		});
	});

	private final HttpHandler next;
	private final Filter delegate;
	private Policy policy;
	private Predicate<URI> uriPredicate;

	public CorsFilter(HttpHandler next) {
		LOG_INIT.get();
		this.next = next;
		this.delegate = new Filter(next) {

			@Override
			public Policy createPolicy(String name, String param) {
				if (policy == null)
					return super.createPolicy(name, param);
				return policy;
			}
		};
		this.delegate.setUrlPattern(".*");
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		if (uriPredicate != null && !uriPredicate.test(UndertowUtils.getRequestURI(exchange))) {
			this.next.handleRequest(exchange);
			return;
		}
		this.delegate.handleRequest(exchange);
	}

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public Predicate<URI> getUriPredicate() {
		return uriPredicate;
	}

	public void setUriPredicate(Predicate<URI> uriPredicate) {
		this.uriPredicate = uriPredicate;
	}

	// delegates

	/**
	 * Sets the {@code exposeHeaders}.
	 * 
	 * <p>
	 * This method is called by Wildfly / JBoss EAP based on the config in
	 * standalone.xml.
	 * </p>
	 * 
	 * <p>
	 * To configure this parameter, specify a <code>param</code> element in the
	 * <code>filter</code> definition, like this:
	 * </p>
	 * 
	 * <pre>
	 * <code>
	&lt;filters&gt;
	&lt;filter name="cors-filter" class-name="com.stijndewitt.undertow.cors.Filter" module="com.stijndewitt.undertow.cors"&gt;
	...
	&lt;param name="exposeHeaders" value="Accept-Ranges,Content-Length,Content-Range,ETag,Link,Server,X-Total-Count" /&gt;
	...
	&lt;/filter&gt;
	&lt;/filters&gt;
	</code>
	 * </pre>
	 * 
	 * @param value The new value for the header, possibly {@code null}.
	 * 
	 * @see #getExposeHeaders
	 */
	public void setExposeHeaders(String value) {
		delegate.setExposeHeaders(value);
	}

	/**
	 * Gets the configured {@code exposeHeaders}.
	 * 
	 * @return The configured setting, or the default.
	 * 
	 * @see #setExposeHeaders
	 * @see #DEFAULT_EXPOSE_HEADERS
	 */
	public String getExposeHeaders() {
		return delegate.getExposeHeaders();
	}

	/**
	 * Sets the {@code maxAge}.
	 * 
	 * <p>
	 * This method is called by Wildfly / JBoss EAP based on the config in
	 * standalone.xml.
	 * </p>
	 * 
	 * <p>
	 * To configure this parameter, specify a <code>param</code> element in the
	 * <code>filter</code> definition, like this:
	 * </p>
	 * 
	 * <pre>
	 * <code>
	&lt;filters&gt;
	&lt;filter name="cors-filter" class-name="com.stijndewitt.undertow.cors.Filter" module="com.stijndewitt.undertow.cors"&gt;
	...
	&lt;param name="maxAge" value="864000" /&gt;
	...
	&lt;/filter&gt;
	&lt;/filters&gt;
	</code>
	 * </pre>
	 * 
	 * @param value The new value for the header, possibly {@code null}.
	 * 
	 * @see #getMaxAge
	 */
	public void setMaxAge(String value) {
		delegate.setMaxAge(value);
		;
	}

	/**
	 * Gets the configured {@code maxAge}.
	 * 
	 * @return The configured setting, or the default.
	 * 
	 * @see #setMaxAge
	 * @see #DEFAULT_MAX_AGE
	 */
	public String getMaxAge() {
		return delegate.getMaxAge();
	}

	/**
	 * Sets the {@code allowCredentials}.
	 * 
	 * <p>
	 * This method is called by Wildfly / JBoss EAP based on the config in
	 * standalone.xml.
	 * </p>
	 * 
	 * <p>
	 * To configure this parameter, specify a <code>param</code> element in the
	 * <code>filter</code> definition, like this:
	 * </p>
	 * 
	 * <pre>
	 * <code>
	&lt;filters&gt;
	&lt;filter name="cors-filter" class-name="com.stijndewitt.undertow.cors.Filter" module="com.stijndewitt.undertow.cors"&gt;
	...
	&lt;param name="allowCredentials" value="true" /&gt;
	...
	&lt;/filter&gt;
	&lt;/filters&gt;
	</code>
	 * </pre>
	 * 
	 * @param value The new value for the header, possibly {@code null}.
	 * 
	 * @see #getAllowCredentials
	 */
	public void setAllowCredentials(String value) {
		delegate.setAllowCredentials(value);
	}

	/**
	 * Gets the configured {@code allowCredentials}.
	 * 
	 * @return The configured setting, or the default.
	 *
	 * @see #setAllowCredentials
	 * @see #DEFAULT_ALLOW_CREDENTIALS
	 */
	public String getAllowCredentials() {
		return delegate.getAllowCredentials();
	}

	/**
	 * Sets the {@code allowMethods}.
	 * 
	 * <p>
	 * This method is called by Wildfly / JBoss EAP based on the config in
	 * standalone.xml.
	 * </p>
	 * 
	 * <p>
	 * To configure this parameter, specify a <code>param</code> element in the
	 * <code>filter</code> definition, like this:
	 * </p>
	 * 
	 * <pre>
	 * <code>
	&lt;filters&gt;
	&lt;filter name="cors-filter" class-name="com.stijndewitt.undertow.cors.Filter" module="com.stijndewitt.undertow.cors"&gt;
	...
	&lt;param name="allowMethods" value="DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT" /&gt;
	...
	&lt;/filter&gt;
	&lt;/filters&gt;
	</code>
	 * </pre>
	 * 
	 * @param value The new value for the header, possibly {@code null}.
	 * 
	 * @see #getAllowMethods
	 */
	public void setAllowMethods(String value) {
		delegate.setAllowMethods(value);
	}

	/**
	 * Gets the configured {@code allowMethods}.
	 * 
	 * @return The configured setting, or the default.
	 * 
	 * @see #setAllowMethods
	 * @see #DEFAULT_ALLOW_METHODS
	 */
	public String getAllowMethods() {
		return delegate.getAllowMethods();
	}

	/**
	 * Sets the {@code allowHeaders}.
	 * 
	 * <p>
	 * This method is called by Wildfly / JBoss EAP based on the config in
	 * standalone.xml.
	 * </p>
	 * 
	 * <p>
	 * To configure this parameter, specify a <code>param</code> element in the
	 * <code>filter</code> definition, like this:
	 * </p>
	 * 
	 * <pre>
	 * <code>
	&lt;filters&gt;
	&lt;filter name="cors-filter" class-name="com.stijndewitt.undertow.cors.Filter" module="com.stijndewitt.undertow.cors"&gt;
	...
	&lt;param name="allowHeaders" value="Authorization,Content-Type,Link,X-Total-Count,Range" /&gt;
	...
	&lt;/filter&gt;
	&lt;/filters&gt;
	</code>
	 * </pre>
	 * 
	 * @param value The new value for the header, possibly {@code null}.
	 * 
	 * @see #getAllowMethods
	 */
	public void setAllowHeaders(String value) {
		delegate.setAllowHeaders(value);
	}

	/**
	 * Gets the configured {@code allowHeaders}.
	 * 
	 * @return The configured setting, or the default.
	 * 
	 * @see #setAllowMethods
	 * @see #DEFAULT_ALLOW_METHODS
	 */
	public String getAllowHeaders() {
		return delegate.getAllowHeaders();
	}

}
