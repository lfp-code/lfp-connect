package com.lfp.connect.undertow.handler;

import java.net.URI;
import java.util.function.Function;

import com.lfp.joe.net.status.StatusCodes;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class RedirectHandler implements HttpHandler {

	private final Function<HttpServerExchange, URI> redirectURIFunction;
	private final int statusCode;

	public RedirectHandler(URI uri) {
		this(nil -> uri);
	}

	public RedirectHandler(URI uri, int statusCode) {
		this(nil -> uri, statusCode);
	}

	public RedirectHandler(Function<HttpServerExchange, URI> redirectURIFunction) {
		this(redirectURIFunction, StatusCodes.FOUND);
	}

	public RedirectHandler(Function<HttpServerExchange, URI> redirectURIFunction, int statusCode) {
		this.redirectURIFunction = redirectURIFunction;
		this.statusCode = statusCode;
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		var redirectURI = this.redirectURIFunction == null ? null : this.redirectURIFunction.apply(exchange);
		if (redirectURI == null)
			return;
		exchange.setStatusCode(this.statusCode);
		exchange.getResponseHeaders().put(Headers.LOCATION, redirectURI.toString());
		exchange.endExchange();
	}

}
