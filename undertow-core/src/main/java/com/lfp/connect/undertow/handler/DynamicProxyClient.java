package com.lfp.connect.undertow.handler;

import java.io.IOException;
import java.net.URI;
import java.nio.channels.Channel;
import java.nio.channels.ClosedChannelException;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.core.log.LogFilter;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import org.xnio.ChannelListener;
import org.xnio.IoUtils;
import org.xnio.OptionMap;

import ch.qos.logback.core.spi.FilterReply;
import io.undertow.client.ClientCallback;
import io.undertow.client.ClientConnection;
import io.undertow.client.UndertowClient;
import io.undertow.protocols.ssl.UndertowXnioSsl;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.ServerConnection;
import io.undertow.server.handlers.proxy.ProxyCallback;
import io.undertow.server.handlers.proxy.ProxyClient;
import io.undertow.server.handlers.proxy.ProxyConnection;
import io.undertow.util.AttachmentKey;

public abstract class DynamicProxyClient implements ProxyClient {
	private static final MemoizedSupplier<Void> LOG_INIT = Utils.Functions.memoize(() -> {
		LogFilter.addFilter(ftx -> {
			if (!Utils.Strings.equals(ftx.getEvent().getLoggerName(), "io.undertow.proxy"))
				return FilterReply.NEUTRAL;
			var className = Optional.ofNullable(ftx.getEvent()).map(v -> v.getThrowableProxy())
					.map(v -> v.getClassName()).orElse(null);
			if (ClosedChannelException.class.getName().equals(className))
				return FilterReply.DENY;
			return FilterReply.NEUTRAL;
		});

	});

	public static DynamicProxyClient create(Function<HttpServerExchange, URI> upstreamURIFunction) {
		Objects.requireNonNull(upstreamURIFunction);
		return new DynamicProxyClient() {

			@Override
			protected URI getUpstreamURI(HttpServerExchange exchange) {
				return upstreamURIFunction.apply(exchange);
			}
		};
	}

	private final AttachmentKey<UpstreamContext> upstreamContextKey = AttachmentKey.create(UpstreamContext.class);
	private final UndertowClient client;

	private static final ProxyTarget TARGET = new ProxyTarget() {
	};

	public DynamicProxyClient() {
		LOG_INIT.get();
		this.client = UndertowClient.getInstance();
	}

	@Override
	public ProxyTarget findTarget(HttpServerExchange exchange) {
		return TARGET;
	}

	@Override
	public void getConnection(ProxyTarget target, HttpServerExchange exchange, ProxyCallback<ProxyConnection> callback,
			long timeout, TimeUnit timeUnit) {
		UpstreamContext existing = exchange.getConnection().getAttachment(upstreamContextKey);
		if (existing != null) {
			ClientConnection clientConnection = existing.clientConnection;
			if (clientConnection.isOpen()) {
				// this connection already has a client, re-use it
				callback.completed(exchange, new ProxyConnection(clientConnection, existing.getPath()));
				return;
			} else {
				exchange.getConnection().removeAttachment(upstreamContextKey);
			}
		}
		URI upstreamURI = getUpstreamURI(exchange);
		if (upstreamURI == null)
			return;
		UndertowXnioSsl ssl;	
		if (URIs.isSecure(upstreamURI))
			ssl = Utils.Functions.unchecked(() -> new UndertowXnioSsl(exchange.getConnection().getWorker().getXnio(),
					OptionMap.EMPTY, exchange.getConnection().getByteBufferPool()));
		else
			ssl = null;
		client.connect(new ConnectNotifier(upstreamContextKey, exchange, callback, upstreamURI),
				getConnectionURI(exchange, upstreamURI), exchange.getIoThread(), ssl,
				exchange.getConnection().getByteBufferPool(), OptionMap.EMPTY);
	}

	protected URI getConnectionURI(HttpServerExchange exchange, URI upstreamURI) {
		return upstreamURI;
	}

	protected abstract URI getUpstreamURI(HttpServerExchange exchange);

	protected static final class ConnectNotifier implements ClientCallback<ClientConnection> {
		private final AttachmentKey<UpstreamContext> clientAttachmentKey;
		private final ProxyCallback<ProxyConnection> callback;
		private final HttpServerExchange exchange;
		private final URI upstreamURI;

		private ConnectNotifier(AttachmentKey<UpstreamContext> clientAttachmentKey, HttpServerExchange exchange,
				ProxyCallback<ProxyConnection> callback, URI upstreamURI) {
			this.clientAttachmentKey = clientAttachmentKey;
			this.callback = callback;
			this.exchange = exchange;
			this.upstreamURI = upstreamURI;
		}

		@Override
		public void completed(final ClientConnection connection) {
			final ServerConnection serverConnection = exchange.getConnection();
			UpstreamContext upstreamContext = new UpstreamContext(connection, upstreamURI);
			// we attach to the connection so it can be re-used
			serverConnection.putAttachment(clientAttachmentKey, upstreamContext);
			serverConnection.addCloseListener(new ServerConnection.CloseListener() {
				@Override
				public void closed(ServerConnection serverConnection) {
					upstreamContext.scrap();
				}
			});
			connection.getCloseSetter().set(new ChannelListener<Channel>() {
				@Override
				public void handleEvent(Channel channel) {
					serverConnection.removeAttachment(clientAttachmentKey);
				}
			});
			callback.completed(exchange, new ProxyConnection(connection, upstreamContext.getPath()));
		}

		@Override
		public void failed(IOException e) {
			callback.failed(exchange);
		}
	}

	public void clearUpstreamContext(HttpServerExchange httpServerExchange) {
		Objects.requireNonNull(httpServerExchange);
		UpstreamContext upstreamContext = httpServerExchange.getConnection().removeAttachment(upstreamContextKey);
		if (upstreamContext == null)
			return;
		upstreamContext.scrap();
	}

	protected static class UpstreamContext implements Scrapable {

		private final Scrapable _scrapable = Scrapable.create();
		private final ClientConnection clientConnection;
		private final URI uri;

		public UpstreamContext(ClientConnection clientConnection, URI uri) {
			super();
			this.clientConnection = Objects.requireNonNull(clientConnection);
			this.uri = Objects.requireNonNull(uri);
			this.onScrap(() -> {
				IoUtils.safeClose(clientConnection);
			});
		}

		public String getPath() {
			String path = uri.getPath();
			if (path == null || path.isEmpty())
				return "/";
			return path;
		}

		@Override
		public boolean isScrapped() {
			return _scrapable.isScrapped();
		}

		@Override
		public Scrapable onScrap(Runnable task) {
			return _scrapable.onScrap(task);
		}

		@Override
		public boolean scrap() {
			return _scrapable.scrap();
		}

	}

}
