package com.lfp.connect.undertow.handler;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import com.google.common.net.MediaType;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;

public class MessageHandler implements HttpHandler {

	private final int code;
	private final String message;

	public MessageHandler(int code) {
		this(code, null);
	}

	public MessageHandler(int code, String message) {
		this.code = code;
		this.message = Optional.ofNullable(message).orElseGet(() -> StatusCodes.getReason(code));
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		if (exchange.isComplete())
			return;
		exchange.setStatusCode(code);
		var responseEntry = getResponseEntry(this.message, UndertowUtils.streamHeaders(exchange.getRequestHeaders()));
		var responseBody = responseEntry.getKey();
		var responseHeaders = responseEntry.getValue();
		for (var ent : responseHeaders) {
			exchange.getResponseHeaders().remove(ent.getKey());
			exchange.getResponseHeaders().put(HttpString.tryFromString(ent.getKey()), ent.getValue());
		}
		exchange.getResponseSender().send(responseBody.buffer());
	}

	public static Entry<Bytes, HeaderMap> getResponseEntry(int statusCode,
			Iterable<? extends Entry<String, String>> headerEntries) {
		return getResponseEntry(StatusCodes.getReason(statusCode), headerEntries);
	}

	public static Entry<Bytes, HeaderMap> getResponseEntry(String message,
			Iterable<? extends Entry<String, String>> requestHeaders) {
		if (Utils.Strings.isBlank(message))
			message = "";
		boolean htmlAccepted = Headers.isHtmlAccepted(requestHeaders);
		MediaType mediaType;
		Bytes body;
		{// body
			if (htmlAccepted) {
				mediaType = MediaType.PLAIN_TEXT_UTF_8;
				body = Bytes.from(message);
			} else {
				mediaType = MediaType.JSON_UTF_8;
				body = Serials.Gsons.toBytes(new Body(message));
			}
		}
		Map<String, String> headerMap;
		{
			headerMap = new LinkedHashMap<>(2);
			headerMap.put(Headers.CONTENT_TYPE, mediaType.toString());
			headerMap.put(Headers.CONTENT_LENGTH, body.length() + "");
		}
		return Utils.Lots.entry(body, HeaderMap.of(headerMap.entrySet()));
	}

	public static class Body {

		private final String message;

		public Body(String message) {
			super();
			this.message = Utils.Strings.trimToNullOptional(message).orElseGet(() -> StatusCodes.getReason(-1));
		}

		public String getMessage() {
			return message;
		}

	}

}
