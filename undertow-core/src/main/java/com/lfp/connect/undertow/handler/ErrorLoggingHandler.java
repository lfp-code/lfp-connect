package com.lfp.connect.undertow.handler;

import java.net.URI;
import java.util.Objects;

import org.slf4j.Logger;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.utils.Utils;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;

public class ErrorLoggingHandler implements HttpHandler {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static Logger _logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final HttpHandler httpHandler;
	private final Logger logger;

	public ErrorLoggingHandler(HttpHandler httpHandler) {
		this(httpHandler, null);
	}

	public ErrorLoggingHandler(HttpHandler httpHandler, Logger logger) {
		this.httpHandler = Objects.requireNonNull(httpHandler);
		this.logger = logger != null ? logger : _logger;
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		try {
			httpHandler.handleRequest(exchange);
		} catch (Throwable t) {
			URI requestURI = UndertowUtils.getRequestURI(exchange);
			HttpString method = exchange.getRequestMethod();
			logger.error("uncaught error during exchange. requestURI:{} method:{}", requestURI, method, t);
			exchange.endExchange();
			throw Utils.Exceptions.as(t, Exception.class);
		}

	}

}
