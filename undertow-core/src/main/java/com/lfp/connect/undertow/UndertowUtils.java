package com.lfp.connect.undertow;

import java.io.IOException;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.channels.Channel;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import org.xnio.IoFuture;
import org.xnio.IoFuture.Status;

import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.classpath.ImmutableMemberCache.FieldRequest;
import com.lfp.joe.core.classpath.MemberCache;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.joe.utils.Utils;

import io.mikael.urlbuilder.UrlBuilder;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.server.handlers.CookieImpl;
import io.undertow.server.handlers.PathHandler;
import io.undertow.server.handlers.proxy.LoadBalancingProxyClient.Host;
import io.undertow.server.handlers.proxy.ProxyConnectionPool;
import io.undertow.util.AttachmentKey;
import io.undertow.util.HeaderMap;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import io.undertow.websockets.core.CloseMessage;
import io.undertow.websockets.core.WebSocketCallback;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.spi.AsyncWebSocketHttpServerExchange;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class UndertowUtils {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final HttpString X_FORWARDED_URI = HttpString.tryFromString("X-Forwarded-Uri");

	public static boolean isSecure(HttpServerExchange exchange) {
		return isSecure(exchange, false);
	}

	public static boolean isSecure(HttpServerExchange exchange, boolean disableForwardHeaders) {
		Objects.requireNonNull(exchange);
		var scheme = UndertowUtils.getScheme(exchange, disableForwardHeaders);
		return URIs.isSecure(scheme);
	}

	public static String getScheme(HttpServerExchange exchange) {
		return getScheme(exchange, false);
	}

	public static String getScheme(HttpServerExchange exchange, boolean disableForwardHeaders) {
		Objects.requireNonNull(exchange);
		StreamEx<String> stream = Streams.empty();
		var headerMap = disableForwardHeaders ? null : exchange.getRequestHeaders();
		if (headerMap != null) {
			stream = stream.append(Streams.of(() -> headerMap.eachValue(Headers.X_FORWARDED_PROTO)));
			stream = stream.append(Streams.of(() -> headerMap.eachValue(Headers.X_FORWARDED_FOR)).map(v -> {
				var splitAt = Utils.Strings.indexOf(v, "://");
				return splitAt < 0 ? null : v.substring(0, splitAt);
			}));

		}
		stream = stream.append(exchange.getRequestScheme());
		// stream = stream.peek(System.out::println);
		stream = stream.mapPartial(Utils.Strings::trimToNullOptional)
				.filter(v -> !Utils.Strings.containsWhitespace(v))
				.map(String::toLowerCase)
				.sortedBy(v -> URIs.isSecure(v) ? 0 : 1);
		return stream.findFirst().orElse(null);

	}

	public static URI getRequestURI(HttpServerExchange exchange) {
		return getRequestURI(exchange, false);
	}

	public static URI getRequestURI(HttpServerExchange exchange, boolean disableForwardHeaders) {
		return getRequestUrlbuilder(exchange, disableForwardHeaders).toUri();
	}

	public static UrlBuilder getRequestUrlbuilder(HttpServerExchange exchange) {
		return getRequestUrlbuilder(exchange, false);
	}

	public static UrlBuilder getRequestUrlbuilder(HttpServerExchange exchange, boolean disableForwardHeaders) {
		Objects.requireNonNull(exchange);
		var requestURL = exchange.getRequestURL();
		var queryString = exchange.getQueryString();
		var urlb = UrlBuilder.fromString(requestURL).withQuery(queryString);
		if (!disableForwardHeaders) {
			{// parse scheme
				String value = UndertowUtils.getScheme(exchange);
				if (value != null)
					urlb = urlb.withScheme(value);
			}
			{// parse host
				var value = Streams.of(exchange.getRequestHeaders().eachValue(Headers.X_FORWARDED_HOST))
						.filter(Utils.Strings::isNotBlank)
						.findFirst()
						.orElse(null);
				if (value != null)
					urlb = urlb.withHost(value);
			}
			{// parse port
				var value = Streams.of(exchange.getRequestHeaders().eachValue(Headers.X_FORWARDED_PORT))
						.mapPartial(Utils.Strings::parseNumber)
						.map(Number::intValue)
						.findFirst()
						.orElse(null);
				if (value != null) {
					value = URIs.normalizePort(urlb.scheme, value);
					urlb = urlb.withPort(value < 0 ? null : value);
				}
			}
			{// parse path & query
				var value = Streams.of(exchange.getRequestHeaders().eachValue(X_FORWARDED_URI))
						.filter(Utils.Strings::isNotBlank)
						.findFirst()
						.orElse(null);
				if (value != null) {
					var splitAt = value.indexOf('?');
					Function<String, String> decoder = encoded -> Optional.ofNullable(encoded)
							.map(v -> URLDecoder.decode(v, MachineConfig.getDefaultCharset()))
							.orElse(null);
					var path = decoder.apply(splitAt < 0 ? value : value.substring(0, splitAt));
					var query = decoder.apply(splitAt < 0 ? null : value.substring(splitAt + 1, value.length()));
					urlb = urlb.withPath(path);
					urlb = urlb.withQuery(query);
				}
			}
		}
		return urlb;
	}

	public static com.lfp.joe.net.http.headers.HeaderMap headerMap(HeaderMap headerMap) {
		return com.lfp.joe.net.http.headers.HeaderMap.of(streamHeaders(headerMap));
	}

	public static EntryStream<String, String> streamHeaders(HeaderMap headerMap) {
		if (headerMap == null)
			return EntryStream.empty();
		var httpNameValue = Streams.of(headerMap).flatMap(headerValues -> {
			return Streams.of(headerValues).mapToEntry(nil -> headerValues.getHeaderName()).invert();
		}).chain(EntryStreams::of).nonNullKeys().nonNullValues();
		return httpNameValue.mapKeys(Objects::toString);
	}

	public static EntryStream<String, String> streamQueryParameters(HttpServerExchange exchange) {
		if (exchange == null)
			return EntryStream.empty();
		return EntryStreams.ofMultimap(exchange.getQueryParameters());
	}

	public static Optional<HttpServerExchange> getHttpServerExchange(WebSocketHttpExchange exchange) {
		Objects.requireNonNull(exchange);
		return CoreReflections.tryCast(exchange, AsyncWebSocketHttpServerExchange.class).map(v -> {
			var req = FieldRequest.of(AsyncWebSocketHttpServerExchange.class, HttpServerExchange.class, "exchange");
			return MemberCache.getFieldValue(req, v);
		});
	}

	private static final AttachmentKey<Boolean> WEBSOCKET_ATTACHMENT_KEY = AttachmentKey.create(Boolean.class);;

	public static boolean isWebSocket(HttpServerExchange exchange) {
		if (Boolean.TRUE.equals(exchange.getAttachment(WEBSOCKET_ATTACHMENT_KEY)))
			return true;
		if (Boolean.TRUE.equals(exchange.getConnection().getAttachment(WEBSOCKET_ATTACHMENT_KEY)))
			return true;
		boolean isWebSocket;
		if ("ws".equals(exchange.getRequestScheme()))
			isWebSocket = true;
		else if ("wss".equals(exchange.getRequestScheme()))
			isWebSocket = true;
		else if ("websocket".equalsIgnoreCase(exchange.getRequestHeaders().getFirst(Headers.UPGRADE)))
			isWebSocket = true;
		else
			isWebSocket = false;
		if (!isWebSocket)
			return false;
		exchange.putAttachment(WEBSOCKET_ATTACHMENT_KEY, true);
		exchange.getConnection().putAttachment(WEBSOCKET_ATTACHMENT_KEY, true);
		return true;

	}

	public static boolean isHtmlAccepted(HttpServerExchange exchange) {
		EntryStream<String, String> estream = streamHeaders(exchange == null ? null : exchange.getRequestHeaders());
		return com.lfp.joe.net.http.headers.Headers.isHtmlAccepted(estream);
	}

	public static Cookie toCookie(HttpCookie httpCookie) {
		return toCookie(httpCookie == null ? null : com.lfp.joe.net.cookie.Cookie.build(httpCookie));
	}

	public static Cookie toCookie(com.lfp.joe.net.cookie.Cookie jcookie) {
		Objects.requireNonNull(jcookie);
		CookieImpl cookie = new CookieImpl(jcookie.getName(), jcookie.getValue());
		Optional<Date> expiresAt = jcookie.getExpiresAt();
		if (expiresAt.isPresent())
			cookie.setExpires(expiresAt.get());
		else
			cookie.setMaxAge(-1);
		// String path;
		cookie.setPath(jcookie.getPath());
		// String domain;
		cookie.setDomain(jcookie.getDomain());
		// boolean secure;
		cookie.setSecure(jcookie.isSecure());
		// boolean httpOnly;
		cookie.setHttpOnly(jcookie.isHttpOnly());
		// int version = 0;
		cookie.setVersion(jcookie.getVersion());
		// String comment;
		cookie.setComment(jcookie.getComment());
		return cookie;
	}

	public static <X> CompletableFuture<X> asCompletableFuture(IoFuture<X> future) {
		Objects.requireNonNull(future);
		if (future.getStatus() != Status.WAITING) {
			try {
				return CompletableFuture.completedFuture(future.get());
			} catch (CancellationException | IOException e) {
				return CompletableFuture.failedFuture(e);
			}
		}
		var cfuture = new CompletableFuture<X>();
		future.addNotifier((nil1, nil2) -> {
			if (cfuture.isDone())
				return;
			if (future.getStatus() == Status.CANCELLED)
				cfuture.cancel(true);
			else
				try {
					cfuture.complete(future.get());
				} catch (CancellationException | IOException e) {
					cfuture.completeExceptionally(e);
				}
		}, null);
		cfuture.whenComplete((v, t) -> {
			if (future.getStatus() != Status.WAITING)
				return;
			future.cancel();
		});
		return cfuture;
	}

	private static final CloseMessage NORMAL_CLOSURE_MESSAGE = new CloseMessage(CloseMessage.NORMAL_CLOSURE,
			"normal closure");

	public static boolean sendCloseBlockingQuietly(WebSocketChannel channel) {
		return sendCloseBlockingQuietly(NORMAL_CLOSURE_MESSAGE, channel);
	}

	public static boolean sendCloseBlockingQuietly(CloseMessage closeMessage, WebSocketChannel channel) {
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		sendCloseQuietly(closeMessage, channel, (c, result) -> future.complete(result));
		try {
			return future.get();
		} catch (InterruptedException e) {
			// suppress
			return false;
		} catch (ExecutionException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
	}

	public static void sendCloseQuietly(WebSocketChannel channel, BiConsumer<Channel, Boolean> callback) {
		sendCloseQuietly(NORMAL_CLOSURE_MESSAGE, channel, callback);
	}

	public static void sendCloseQuietly(CloseMessage closeMessage, WebSocketChannel channel,
			BiConsumer<Channel, Boolean> callback) {
		Objects.requireNonNull(closeMessage);
		if (channel == null)
			callback.accept(channel, false);
		if (!channel.isOpen() || channel.isCloseFrameSent() || channel.isCloseFrameReceived()
				|| channel.isCloseInitiatedByRemotePeer())
			callback.accept(channel, false);
		Consumer<Throwable> onError = t -> {
			logger.trace("error during websocket quiet close. closeMessage:{}", closeMessage, t);
		};
		try {
			WebSockets.sendClose(closeMessage, channel, new WebSocketCallback<Void>() {

				@Override
				public void onError(WebSocketChannel channel, Void context, Throwable throwable) {
					onError.accept(throwable);
					callback.accept(channel, false);
				}

				@Override
				public void complete(WebSocketChannel channel, Void context) {
					callback.accept(channel, true);
				}
			});
		} catch (Exception e) {
			onError.accept(e);
			callback.accept(channel, false);
		}
	}

	public static ProxyConnectionPool getProxyConnectionPool(Host host) {
		Objects.requireNonNull(host);
		return MemberCache.getFieldValue(FieldRequest.of(Host.class, ProxyConnectionPool.class, "connectionPool"),
				host);
	}

	public static <X> X loadAttachment(HttpServerExchange hse, AttachmentKey<X> attachmentKey, Supplier<X> loader) {
		var result = hse.getAttachment(attachmentKey);
		if (result == null)
			synchronized (hse) {
				result = hse.getAttachment(attachmentKey);
				if (result == null) {
					result = loader.get();
					hse.putAttachment(attachmentKey, result);
				}

			}
		return result;
	}

	public static PathHandler appendPath(PathHandler pathHandler, String path, HttpHandler handler) {
		Objects.requireNonNull(pathHandler);
		Objects.requireNonNull(handler);
		path = URIs.normalizePath(path);
		String exact;
		String prefix;
		if (Utils.Strings.equals(path, "/")) {
			exact = path;
			prefix = path;
		} else if (Utils.Strings.endsWith(path, "/")) {
			exact = path.substring(0, path.length() - 1);
			prefix = path;
		} else {
			exact = path;
			prefix = path + "/";
		}
		pathHandler.addExactPath(exact, handler);
		pathHandler.addPrefixPath(prefix, handler);
		return pathHandler;
	}

	public static void main(String[] args) {
		System.out.println(Headers.X_FORWARDED_HOST);
		System.out.println(HttpString.tryFromString("x-forwarded-host"));
		System.out.println(Headers.X_FORWARDED_HOST.equals("x-forwarded-host"));
	}
}