package com.lfp.connect.undertow.handler;

import static io.undertow.server.handlers.proxy.ProxyConnectionPool.AvailabilityType.*;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import io.undertow.client.UndertowClient;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.proxy.ExclusivityChecker;
import io.undertow.server.handlers.proxy.LoadBalancingProxyClient;
import io.undertow.server.handlers.proxy.ProxyConnectionPool;
import io.undertow.util.AttachmentKey;
import io.undertow.util.AttachmentList;

@SuppressWarnings("unchecked")
public class LoadBalancingProxyClientLFP extends LoadBalancingProxyClient {

	private BiFunction<HttpServerExchange, Host[], Host> hostSelector = createDefaultHostSelector();

	public LoadBalancingProxyClientLFP() {
		this(null, null, null);
	}

	public LoadBalancingProxyClientLFP(UndertowClient client) {
		this(client, null, null);
	}

	public LoadBalancingProxyClientLFP(BiFunction<HttpServerExchange, Host[], Host> hostSelector) {
		this(null, null, hostSelector);
	}

	public LoadBalancingProxyClientLFP(ExclusivityChecker client) {
		super(null, client, null);
	}

	public LoadBalancingProxyClientLFP(UndertowClient client, ExclusivityChecker exclusivityChecker) {
		this(client, exclusivityChecker, null);
	}

	public LoadBalancingProxyClientLFP(UndertowClient client, ExclusivityChecker exclusivityChecker,
			BiFunction<HttpServerExchange, Host[], Host> hostSelector) {
		super(client != null ? client : UndertowClient.getInstance(), exclusivityChecker, null);
		this.setHostSelector(hostSelector);
	}

	public BiFunction<HttpServerExchange, Host[], Host> getHostSelector() {
		return hostSelector;
	}

	protected void setHostSelector(BiFunction<HttpServerExchange, Host[], Host> hostSelector) {
		if (hostSelector == null)
			hostSelector = createDefaultHostSelector();
		this.hostSelector = hostSelector;
	}

	@Override
	protected Host selectHost(HttpServerExchange exchange) {
		AttachmentList<Host> attempted = exchange.getAttachment(getAttemptedHosts());
		Host[] hosts = this.getHosts();
		if (hosts.length == 0) {
			return null;
		}

		Iterator<CharSequence> parsedRoutes = parseRoutes(exchange);
		while (parsedRoutes.hasNext()) {
			// Attempt to find the first existing host which was not yet attempted
			Host host = this.getRoutes().get(parsedRoutes.next().toString());
			if (host != null) {
				if (attempted == null || !attempted.contains(host)) {
					return host;
				}
			}
		}

		int host = this.selectHost(exchange, hosts);
		if (host == -1)
			return null;
		final int startHost = host; // if the all hosts have problems we come back to this one
		Host full = null;
		Host problem = null;
		do {
			Host selected = hosts[host];
			if (attempted == null || !attempted.contains(selected)) {
				ProxyConnectionPool.AvailabilityType available = UndertowUtils.getProxyConnectionPool(selected)
						.available();
				if (available == AVAILABLE) {
					return selected;
				} else if (available == FULL && full == null) {
					full = selected;
				} else if ((available == PROBLEM || available == FULL_QUEUE) && problem == null) {
					problem = selected;
				}
			}
			host = (host + 1) % hosts.length;
		} while (host != startHost);
		if (full != null) {
			return full;
		}
		if (problem != null) {
			return problem;
		}
		// no available hosts
		return null;
	}

	private int selectHost(HttpServerExchange exchange, Host[] hosts) {
		var host = this.hostSelector.apply(exchange, hosts);
		if (host == null)
			return -1;
		for (int i = 0; i < hosts.length; i++) {
			if (Objects.equals(host, hosts[i]))
				return i;
		}
		return -1;
	}

	private static BiFunction<HttpServerExchange, Host[], Host> createDefaultHostSelector() {
		return new BiFunction<HttpServerExchange, Host[], Host>() {

			private final AtomicInteger indexTracker = new AtomicInteger(-1);

			@Override
			public Host apply(HttpServerExchange hse, Host[] hosts) {
				if (hosts == null || hosts.length == 0)
					return null;
				synchronized (indexTracker) {
					var index = indexTracker.incrementAndGet();
					if (index >= hosts.length) {
						indexTracker.set(-1);
						index = indexTracker.incrementAndGet();
					}
					return hosts[index];
				}
			}
		};
	}

	// reflection nonsense

	private static final Field hosts_FIELD = JavaCode.Reflections.getFieldUnchecked(LoadBalancingProxyClient.class,
			false, f -> "hosts".equals(f.getName()));

	protected Host[] getHosts() {
		return (Host[]) Utils.Functions.unchecked(() -> hosts_FIELD.get(this));
	}

	private static final Field routes_FIELD = JavaCode.Reflections.getFieldUnchecked(LoadBalancingProxyClient.class,
			false, f -> "routes".equals(f.getName()), f -> Map.class.isAssignableFrom(f.getType()));

	protected Map<String, Host> getRoutes() {
		return (Map<String, Host>) Utils.Functions.unchecked(() -> routes_FIELD.get(this));
	}

	private static final Field ATTEMPTED_HOSTS = JavaCode.Reflections.getFieldUnchecked(LoadBalancingProxyClient.class,
			false, f -> "ATTEMPTED_HOSTS".equals(f.getName()), f -> AttachmentKey.class.isAssignableFrom(f.getType()));

	protected AttachmentKey<AttachmentList<Host>> getAttemptedHosts() {
		return (AttachmentKey<AttachmentList<Host>>) Utils.Functions.unchecked(() -> ATTEMPTED_HOSTS.get(this));
	}
}
