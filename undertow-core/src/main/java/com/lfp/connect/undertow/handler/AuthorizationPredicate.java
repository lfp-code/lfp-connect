package com.lfp.connect.undertow.handler;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.connect.undertow.config.UndertowConfig;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import io.undertow.predicate.Predicate;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.AttachmentKey;
import io.undertow.util.Headers;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public interface AuthorizationPredicate extends Predicate {

	public static final AttachmentKey<String> AUTHORIZATION_ATTACHMENT_KEY = AttachmentKey.create(String.class);

	@Override
	default boolean resolve(final HttpServerExchange hse) {
		StreamEx<Candidate> candidateStream = StreamEx.empty();
		{// header stream
			EntryStream<String, String> estream = UndertowUtils.streamHeaders(hse.getRequestHeaders());
			var stream = Candidate.stream(estream);
			stream = headerCandidateFilter(stream);
			candidateStream = candidateStream.append(stream);
		}
		{// cookie stream
			EntryStream<String, String> estream = Utils.Lots
					.stream(hse.getRequestCookies() == null ? null : hse.getRequestCookies().values())
					.mapToEntry(v -> v.getName(), v -> v.getValue());
			var stream = Candidate.stream(estream);
			stream = cookieCandidateFilter(stream);
			candidateStream = candidateStream.append(stream);
		}
		{// query stream
			EntryStream<String, String> estream = UndertowUtils.streamQueryParameters(hse);
			var stream = Candidate.stream(estream);
			stream = queryCandidateFilter(stream);
			candidateStream = candidateStream.append(stream);
		}
		{// attachment key
			var value = hse.getAttachment(AUTHORIZATION_ATTACHMENT_KEY);
			EntryStream<String, String> estream = EntryStream.of(Headers.AUTHORIZATION.toString(), value);
			var stream = Candidate.stream(estream);
			candidateStream = candidateStream.append(stream);
		}
		candidateStream = candidateStream.nonNull();
		candidateStream = candidateStream.filter(v -> Utils.Strings.isNotBlank(v.value()));
		return candidateStream.filter(v -> isAuthorized(hse, v)).findFirst().isPresent();
	}

	default StreamEx<Candidate> headerCandidateFilter(StreamEx<Candidate> candidates) {
		var keys = Configs.get(UndertowConfig.class).authenticationKeys();
		return Candidate.filter(candidates, keys, 1);
	}

	default StreamEx<Candidate> cookieCandidateFilter(StreamEx<Candidate> candidates) {
		var keys = Configs.get(UndertowConfig.class).authenticationKeys();
		return Candidate.filter(candidates, keys, 1);
	}

	default StreamEx<Candidate> queryCandidateFilter(StreamEx<Candidate> candidates) {
		return StreamEx.empty();
	}

	boolean isAuthorized(HttpServerExchange httpServerExchange, Candidate candidate);

	public static interface Candidate {

		String name();

		int nameIndex();

		String value();

		public static StreamEx<Candidate> stream(EntryStream<String, String> estream) {
			if (estream == null)
				return StreamEx.empty();
			estream = estream.nonNullKeys();
			estream = estream.filterValues(Utils.Strings::isNotBlank);
			Map<String, Integer> indexTracking = new HashMap<>();
			return estream.map(ent -> {
				var name = ent.getKey();
				var value = ent.getValue();
				int nameIndex;
				synchronized (indexTracking) {
					nameIndex = indexTracking.compute(name, (k, v) -> v == null ? 0 : v + 1);
				}
				return new Candidate() {

					@Override
					public String name() {
						return name;
					}

					@Override
					public int nameIndex() {
						return nameIndex;
					}

					@Override
					public String value() {
						return value;
					}
				};
			});
		}

		static StreamEx<Candidate> filter(StreamEx<Candidate> candidates, Collection<String> keys, int maxPerKey) {
			candidates = candidates.filter(v -> {
				if (keys == null || keys.isEmpty())
					return true;
				for (var key : keys) {
					if (Utils.Strings.isBlank(key))
						continue;
					if (Utils.Strings.equalsIgnoreCase(v.name(), key))
						return true;
				}
				return false;
			});
			candidates = candidates.filter(v -> maxPerKey == -1 ? true : v.nameIndex() < maxPerKey);
			return candidates;
		}

	}

}
