package com.lfp.connect.undertow;

import java.io.File;
import java.net.InetSocketAddress;
import java.net.URI;
import java.time.Duration;
import java.util.function.Supplier;

import org.xnio.IoFuture;
import org.xnio.OptionMap;

import com.lfp.connect.undertow.config.UndertowConfig;
import com.lfp.connect.undertow.ssl.UndertowSSLContext;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import io.undertow.Undertow;
import io.undertow.client.ClientConnection;
import io.undertow.client.UndertowClient;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.AttachmentKey;

public class Undertows {

	public static Undertow.Builder serverBuilder() {
		return serverBuilder(Configs.get(UndertowConfig.class).defaultPort());
	}

	public static Undertow.Builder serverBuilder(int port) {
		return serverBuilder(new InetSocketAddress("0.0.0.0", port));
	}

	public static Undertow.Builder serverBuilder(InetSocketAddress address) {
		int port = address.getPort();
		String host = address.getHostString();
		return serverBuilderBase().addHttpListener(port, host);
	}

	public static Undertow.Builder serverBuilderBase() {
		UndertowConfig undertowConfig = Configs.get(UndertowConfig.class);
		return Undertow.builder()
				.setWorkerThreads(undertowConfig.workerThreadCoreMultiplier() * Utils.Machine.logicalProcessorCount())
				.setByteBufferPool(ConnectionProperties.INSTANCE.getBufferPool());
	}

	public static UndertowSSLContext createSSLContext() {
		UndertowConfig undertowConfig = Configs.get(UndertowConfig.class);
		File privateKey = new File(undertowConfig.defaultSSLPrivateKeyPath());
		File certificate = new File(undertowConfig.defaultSSLCertificatePath());
		return createSSLContext(privateKey, certificate);
	}

	public static UndertowSSLContext createSSLContext(File privateKey, File certificate) {
		return new UndertowSSLContext.FileBacked(privateKey, certificate,
				Duration.ofMillis(Configs.get(UndertowConfig.class).fileBackedSSLContextCacheDurationMillis()));
	}

	public static IoFuture<ClientConnection> clientConnect(URI uri) {
		return clientConnect(uri, null);
	}

	public static IoFuture<ClientConnection> clientConnect(URI uri, OptionMap optionMap) {
		optionMap = optionMap == null ? OptionMap.EMPTY : optionMap;
		return UndertowClient.getInstance().connect(uri, ConnectionProperties.INSTANCE.getWorker(),
				ConnectionProperties.INSTANCE.getBufferPool(), optionMap);
	}
	
	
}
