package com.lfp.connect.undertow;

import java.io.IOException;

import org.xnio.OptionMap;
import org.xnio.Xnio;
import org.xnio.XnioWorker;

import de.xn__ho_hia.storage_unit.Megabyte;
import io.undertow.connector.ByteBufferPool;
import io.undertow.server.DefaultByteBufferPool;

public enum ConnectionProperties {
	INSTANCE;

	private final int DEFAULT_POOL_BUFFER_SIZE = Megabyte.valueOf(512).intValue();
	private final ByteBufferPool bufferPool = new DefaultByteBufferPool(false, DEFAULT_POOL_BUFFER_SIZE);
	private final Xnio xnio;
	private XnioWorker worker;

	private ConnectionProperties() {
		this.xnio = Xnio.getInstance();
		try {
			this.worker = xnio.createWorker(OptionMap.builder().getMap());
		} catch (IllegalArgumentException | IOException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	public ByteBufferPool getBufferPool() {
		return bufferPool;
	}

	public XnioWorker getWorker() {
		return worker;
	}
}
