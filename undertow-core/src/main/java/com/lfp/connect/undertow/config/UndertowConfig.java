package com.lfp.connect.undertow.config;

import java.net.InetSocketAddress;
import java.util.List;

import org.aeonbits.owner.Config;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.properties.converter.InetSocketAddressConverter;

public interface UndertowConfig extends Config {

	@DefaultValue("8080")
	int defaultPort();

	@ConverterClass(InetSocketAddressConverter.class)
	@DefaultValue("0.0.0.0:${defaultPort}")
	InetSocketAddress defaultAddress();

	@DefaultValue("160")
	int workerThreadCoreMultiplier();

	@DefaultValue("1000")
	int fileBackedSSLContextCacheDurationMillis();

	@DefaultValue("/run/secrets/key.key")
	String defaultSSLPrivateKeyPath();

	@DefaultValue("/run/secrets/cert.crt")
	String defaultSSLCertificatePath();

	@DefaultValue("com/lfp/joe/undertow/redirect.html")
	String redirectTemplatePath();

	@DefaultValue(Headers.AUTHORIZATION + ",access_token")
	List<String> authenticationKeys();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.builder().withPrependClassNames(true).withJsonOutput(false).build());
	}

}
