package com.lfp.connect.undertow.handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Supplier;

import com.goebl.david.Request.Method;
import com.lfp.connect.undertow.UndertowUtils;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.net.http.client.HttpURLConnectionResponse;
import com.lfp.joe.net.http.client.HttpURLConnections;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;
import one.util.streamex.EntryStream;

public abstract class RequestInterceptHandler implements HttpHandler {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static RequestInterceptHandler create(Function<URI, URI> modifyRequestURIFunction) {
		Objects.requireNonNull(modifyRequestURIFunction);
		return new RequestInterceptHandler() {

			@Override
			protected URI modifyRequestURI(URI requestURI) {
				return modifyRequestURIFunction.apply(requestURI);
			}
		};
	}

	@Override
	public void handleRequest(HttpServerExchange hsx) throws Exception {
		URI requestURI = modifyRequestURI(UndertowUtils.getRequestURI(hsx));
		Method requestMethod = Utils.Lots.stream(Method.values())
				.filter(v -> v.name().equalsIgnoreCase(hsx.getRequestMethod().toString())).findFirst().orElse(null);
		if (requestMethod == null)
			throw new IOException("unsupported request method:" + hsx.getRequestMethod());
		Map<String, List<String>> requestHeaders;
		{
			EntryStream<String, String> estream = UndertowUtils.streamHeaders(hsx.getRequestHeaders());
			estream = estream.filterKeys(k -> !Headers.HOST.equalsIgnoreCase(k));
			estream = estream.filterKeys(k -> !Headers.CONTENT_LENGTH.equalsIgnoreCase(k));
			requestHeaders = estream.grouping();
		}
		InputStream body = getRequestBody(hsx, requestHeaders);
		HttpURLConnectionResponse response = HttpURLConnections.execute(requestURI, requestMethod, requestHeaders,
				body);
		hsx.setStatusCode(response.getStatusCode());
		for (Entry<String, List<String>> ent : response.getHeaders().entrySet()) {
			String name = ent.getKey();
			if (name == null)
				continue;
			if (Headers.CONTENT_LENGTH.equalsIgnoreCase(name))
				continue;
			List<String> values = Utils.Lots.stream(ent.getValue()).nonNull().toList();
			hsx.getResponseHeaders().addAll(new HttpString(name), values);
		}
		MemoizedSupplier<OutputStream> outputStreamSupplier = Utils.Functions.memoize(() -> {
			hsx.startBlocking();
			return hsx.getOutputStream();
		});
		try (InputStream responseBody = response.getBody()) {
			writeResponse(hsx, response, responseBody, outputStreamSupplier);
		} finally {
			OutputStream os = outputStreamSupplier.getIfLoaded();
			if (os != null)
				os.close();
		}

	}

	protected void writeResponse(HttpServerExchange exchange, HttpURLConnectionResponse response,
			InputStream responseBody, Supplier<OutputStream> outputStreamSupplier) throws IOException {
		if (StatusCodes.NO_CONTENT == response.getStatusCode())
			return;
		Utils.Bits.copy(responseBody, outputStreamSupplier.get());
	}

	protected InputStream getRequestBody(HttpServerExchange exchange, Map<String, List<String>> requestHeaders)
			throws IOException {
		if (Method.GET.name().equalsIgnoreCase(exchange.getRequestMethod().toString()))
			return null;
		exchange.startBlocking();
		InputStream body = exchange.getInputStream();
		if (!MachineConfig.isDeveloper())
			return body;
		Bytes bodyBytes;
		try {
			bodyBytes = Bytes.from(body);
		} finally {
			body.close();
		}
		logger.info("forwarding body:{}", bodyBytes.encodeUtf8());
		return bodyBytes.inputStream();
	}

	protected abstract URI modifyRequestURI(URI requestURI);

}
