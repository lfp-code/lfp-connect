package com.lfp.connect.undertow.handler;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import com.lfp.connect.undertow.config.UndertowConfig;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

public class HtmlRedirectHandler implements HttpHandler {

	private final MemoizedSupplier<Bytes> htmlSupplier;

	public HtmlRedirectHandler(URI redirectURI) {
		Objects.requireNonNull(redirectURI);
		this.htmlSupplier = MemoizedSupplier.create(() -> {
			String html = Utils.Resources
					.getResourceAsString(Configs.get(UndertowConfig.class).redirectTemplatePath()).get();
			html = Utils.Strings.templateApply(html, "REDIRECT_URL", redirectURI);
			return Bytes.from(html.getBytes(StandardCharsets.UTF_8));
		});
	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		exchange.setStatusCode(StatusCodes.FOUND);
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html; charset=UTF-8");
		exchange.getResponseSender().send(htmlSupplier.get().buffer());
	}

}
