package com.lfp.connect.undertow.handler;

import java.net.URI;

import com.lfp.connect.undertow.UndertowUtils;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;

public class HttpsRedirectHandler extends RedirectHandler {

	public HttpsRedirectHandler() {
		super(hse -> getRedirectURI(hse), StatusCodes.TEMPORARY_REDIRECT);
	}

	private static URI getRedirectURI(HttpServerExchange hse) {
		if (UndertowUtils.isSecure(hse))
			return null;
		var urlb = UndertowUtils.getRequestUrlbuilder(hse, false);
		urlb = urlb.withScheme("https");
		return urlb.toUri();
	}

}
