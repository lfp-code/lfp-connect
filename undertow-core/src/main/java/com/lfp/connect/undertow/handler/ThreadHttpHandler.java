package com.lfp.connect.undertow.handler;

import java.util.Objects;
import java.util.concurrent.Executor;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class ThreadHttpHandler implements HttpHandler {

	private final HttpHandler httpHandler;
	private final Executor executor;

	public ThreadHttpHandler(HttpHandler httpHandler) {
		this(httpHandler, null);
	}

	public ThreadHttpHandler(HttpHandler httpHandler, Executor executor) {
		this.httpHandler = Objects.requireNonNull(httpHandler);
		this.executor = executor;

	}

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		if (exchange.isInIoThread()) {
			if (executor == null)
				exchange.dispatch(this);
			else
				exchange.dispatch(executor, this);
			return;
		}
		this.httpHandler.handleRequest(exchange);

	}

}
