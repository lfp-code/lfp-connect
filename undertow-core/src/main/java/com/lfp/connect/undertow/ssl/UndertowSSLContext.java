package com.lfp.connect.undertow.ssl;

import java.io.File;
import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiFunction;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLContextSpi;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.lfp.joe.net.ssl.SSLs;
import com.lfp.joe.utils.Utils;

import one.util.streamex.StreamEx;

public class UndertowSSLContext extends SSLContext {
	private final AtomicReference<BiFunction<String, Integer, SSLEngine>> sslEngineFunctionRef;

	public UndertowSSLContext() {
		this((BiFunction<String, Integer, SSLEngine>) null);
	}

	public UndertowSSLContext(BiFunction<String, Integer, SSLEngine> sslEngineFunction) {
		this(new AtomicReference<>());
		this.setSslEngineFunction(sslEngineFunction);
	}

	private UndertowSSLContext(AtomicReference<BiFunction<String, Integer, SSLEngine>> sslEngineFunctionRef) {
		super(new SSLContextSpi() {

			@Override
			protected SSLEngine engineCreateSSLEngine(String host, int port) {
				BiFunction<String, Integer, SSLEngine> func = sslEngineFunctionRef.get();
				if (func == null)
					return null;
				return func.apply(host, port);
			}

			@Override
			protected SSLEngine engineCreateSSLEngine() {
				throw new UnsupportedOperationException();
			}

			@Override
			protected SSLSessionContext engineGetClientSessionContext() {
				throw new UnsupportedOperationException();
			}

			@Override
			protected SSLSessionContext engineGetServerSessionContext() {
				throw new UnsupportedOperationException();
			}

			@Override
			protected SSLServerSocketFactory engineGetServerSocketFactory() {
				throw new UnsupportedOperationException();
			}

			@Override
			protected SSLSocketFactory engineGetSocketFactory() {
				throw new UnsupportedOperationException();
			}

			@Override
			protected void engineInit(KeyManager[] km, TrustManager[] tm, SecureRandom sr)
					throws KeyManagementException {
				throw new UnsupportedOperationException();
			}
		}, null, null);
		this.sslEngineFunctionRef = sslEngineFunctionRef;
	}

	public BiFunction<String, Integer, SSLEngine> getSslEngineFunction() {
		return sslEngineFunctionRef.get();
	}

	public void setSslEngineFunction(BiFunction<String, Integer, SSLEngine> sslEngineFunction) {
		this.sslEngineFunctionRef.set(sslEngineFunction);
	}

	public static class FileBacked extends UndertowSSLContext {

		private final LoadingCache<Optional<Void>, CompletableFuture<SSLContext>> lookupCache;
		private CompletableFuture<SSLContext> _current = CompletableFuture.completedFuture(null);
		private Long modifiedAt;

		public FileBacked(File privateKey, File publicKey, Duration cacheDuration) {
			super();
			Objects.requireNonNull(privateKey);
			Objects.requireNonNull(publicKey);
			lookupCache = Caffeine.newBuilder().expireAfterWrite(cacheDuration).build(nil -> {
				Long latestModification = StreamEx.of(privateKey.lastModified(), publicKey.lastModified())
						.sortedBy(v -> -1 * v).findFirst().get();
				if (latestModification.equals(modifiedAt))
					return _current;
				modifiedAt = latestModification;
				_current = new CompletableFuture<>();
				try {
					_current.complete(SSLs.createSSLContext(privateKey, publicKey));
				} catch (Throwable t) {
					_current.completeExceptionally(t);
				}
				return _current;
			});
			super.setSslEngineFunction((host, port) -> {
				return Utils.Functions.unchecked(() -> lookupCache.get(Optional.empty()).get()).createSSLEngine(host,
						port);
			});
		}

	}

}
